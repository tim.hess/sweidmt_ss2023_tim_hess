\documentclass[a4paper,12pt]{article}
\usepackage{amssymb} % needed for math
\usepackage{amsmath} % needed for math
\usepackage[utf8]{inputenc} % this is needed for german umlauts
\usepackage[ngerman]{babel} % this is needed for german umlauts
\usepackage[T1]{fontenc}    % this is needed for correct output of umlauts in pdf
\usepackage[margin=2.5cm]{geometry} %layout
\usepackage{float}

% this is needed for forms and links within the text
\usepackage{hyperref}

% The following is needed in order to make the code compatible
% with both latex/dvips and pdflatex.
\usepackage[pdftex]{graphicx}
\DeclareGraphicsRule{*}{mps}{*}{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Variablen                                 						 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\authorName}{Tim Heß}
\newcommand{\auftraggeber}{Lehrstuhl für Medizintechnik der RWTH Aachen}
\newcommand{\projektName}{SWEIDMT SS2023 370477}
\newcommand{\tags}{\authorName, Pflichtenheft, RWTH, Medizintechnik}
\title{\projektName~(Pflichtenheft)}
\author{\authorName}
\date{\today}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PDF Meta information                                 				 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\hypersetup{
    pdfauthor   = {\authorName},
    pdfkeywords = {\tags},
    pdftitle    = {\projektName}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create a shorter version for tables. DO NOT CHANGE               	 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand\addrow[2]{#1 &#2\\ }

\newcommand\addheading[2]{#1 &#2\\ \hline}
\newcommand\tabularhead{\begin{tabular}{lp{13cm}}
\hline
}

\newcommand\addmulrow[2]{ \begin{minipage}[t][][t]{2.5cm}#1\end{minipage}%
&\begin{minipage}[t][][t]{8cm}
\begin{enumerate} #2   \end{enumerate}
\end{minipage}\\ }

\newenvironment{usecase}{\tabularhead}
{\hline\end{tabular}}

\usepackage{microtype}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% THE DOCUMENT BEGINS             	                              	 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\pagenumbering{roman}
\input{Deckblatt}         % Deckblatt.tex laden und einfügen
\setcounter{page}{2}
\tableofcontents          % Inhaltsverzeichnis ausgeben
\clearpage
\pagenumbering{arabic}

\section{Ausgangssituation}

Der Auftragssteller führt regelmäßig Operationen am Hüftgelenk durch. Hierbei ist es notwendig eine Bohrung in den Femurkopf zu setzen um eine Schraube zu platzieren.
Um die Bohrung zu setzen wird ein CT Datensatz des Hüftgelenks benötigt. Dieser Datensatz wird in einem speziellen Format geliefert und muss vor der Operation visualisiert werden.
Zum aktuellen Zeitpunkt wird der Datensatz in einem externen Programm visualisiert und die Bohrung manuell geplant.
Der Auftragssteller wünscht sich eine Software die es ihm ermöglicht den Datensatz zu visualisieren und die Bohrung zu planen, sowie direkt eine passende Bohrschablone zu berechnen.

\section{Aufgabenstellung}

Ziel des Projektes ist es eine Software zu entwickeln die dem Nutzer die Möglichkeit gibt einen CT Datensatz einzuladen und diesen zu visualisieren.
Der Ablauf der Software soll wie folgt sein:

\begin{enumerate}
    \item Der Nutzer lädt einen CT Datensatz ein und visualisiert diesen
    \item Der Nutzer plant die Bohrung
    \item Der Nutzer verifiziert die geplante Bohrung
    \item Die Nutzer plant die Bohrschablone
\end{enumerate}

Die Arbeitsschritte sollten ausschließlich in der Software durchgeführt werden können und nacheinander abgearbeitet werden.
Wenn ein Arbeitsschritt noch nicht abgeschlossen ist, soll der Nutzer nicht in der Lage sein den nächsten Schritt zu beginnen, um fehlerhafte Planungen zu vermeiden.
Auf der anderen Seite soll deutlich gemacht werden, wenn ein Arbeitsschritt abgeschlossen ist und der Nutzer den nächsten Schritt beginnen kann.
Um die Bedienung der Software zu vereinfachen, soll die Software über eine grafische Benutzeroberfläche verfügen. In jedem Arbeitsschritt sollen immer mindestens zwei Ansichten zur Verfügung stehen, um ein vergleichen der Ansichten zu ermöglichen.
Der Bereich der Bedienelemente soll sich immer an der gleichen Stelle befinden, sodass Parameter nicht übersehen werden können.


\subsection{Wunschkriterien}

Im einem Update der Software soll es dem Nutzer möglich sein die Bohrschablone direkt in der Software zu visualisieren zu öffnen.
Ebenfalls soll Anstatt dem ablauf der einzelnen Arbeitsschritte, der Nutzer die Möglichkeit haben die Datei mit dem Start- und Endpunkt der Bohrung sowie den Durchmesser des Bohrkanals zu öffnen um einen vorherigen Planung wiederherzustellen.

\section{Implementierungsentwurf}

Die Software soll in mehrere Komponenten unterteilt werden.
Ein grundlegendes Klassendiagramm ist in Abbildung \ref{fig:klassendiagramm} dargestellt.
Die GUI soll in einer eigener Komponente implementiert werden, welche von der Klasse QWidgets erbt.
Hier soll die Logik der GUI implementiert, welche die einzelnen Arbeitsschritte abarbeitet und die Visualisierung des Datensatzes ermöglicht.

Die notwendigen Funktionalitäten zum einlesen, visualisieren und bearbeiten des Datensatzes sollen in einer eigenen Bibliothek implementiert werden.
Diese stellt alle notwendigen Funktionen zur Verfügung um den Datensatz einzulesen, die Fensterung zu verändern, 3D Darstellung zu erstellen und die Bohrung zu planen.

Zur Kommunikation zwischen der GUI und der Bibliothek stehen mehrere Hilfsklassen zur Verfügung. Hierzu zählen die Klassen ImageData2D und ImageData3D, welche 2D und 3D Bilddaten mit zugehörigen Informationen schachteln.
Das Struct WindowingConfig schachtelt die notwendigen Parameter für die Fensterung des Datensatzes.
Das Enum ReturnCode gibt die verschiedenen Rückgabewerte der Funktionen der Bibliothek an, um Fehler zu signalisieren.

\begin{figure}[h]
\centering
\includegraphics[width=0.8\textwidth]{Klassendiagramm.png}
\caption{Grundlegendes Konzept der GUI}
\label{fig:klassendiagramm}
\end{figure}

Ein grundlegendes Konzept der GUI ist in Abbildung \ref{fig:gui} dargestellt.

\begin{figure}[h]
\centering
\includegraphics[width=0.8\textwidth]{Konzept.png}
\caption{Grundlegendes Konzept der GUI}
\label{fig:gui}
\end{figure}

\section{Funktionale Anforderungen}

Die funktionalen Anforderungen an die Software sind in vier Arbeitsschritte unterteilt.
Jeder Arbeitsschritt soll nacheinander abgearbeitet werden und erst begonnen werden können, wenn der vorherige Arbeitsschritt erfolgreich abgeschlossen wurde.

\subsection{Datensatz einladen und darstellen}

Das einladen und darstellen des Datensatzes soll in einem eigenen Arbeitsschritt erfolgen.
Der Nutzer soll die Möglichkeit haben den Datensatz einzuladen und die Fensterung des Datensatzes zu verändern.
Ebenfalls soll der Nutzer die Möglichkeit haben den Segmentierungs-Threshold zu verändern und die 3D Darstellung des Datensatzes zu erstellen.
Erst wenn der Nutzer erfolgreich einen Datensatz eingelesen und eine 3D Darstellung erstellt hat, soll er den nächsten Arbeitsschritt beginnen können.

\subsubsection{CT Datensatz einladen}

Zum einladen des CT Datensatzes soll der Nutzer die Möglichkeit haben eine .raw Datei einzulesen. Diese soll aus eine beliebigen Anzahl von Schichten bestehen, wobei jede Schicht die feste Größe von 512x512 Pixeln hat.
Zum auswählen des Datensatzen soll ein Datei-Dialog zur Verfügung stehen.
Bei fehlerhaften Dateien, falschen Dateiformaten oder fehlerhaften Dateien soll eine Fehlermeldung ausgegeben werden, die den Nutzer auf das Problem hinweist.
Der Datensatzes soll, sobald er eingeladen ist, in einer 2D Ansicht zur Verfügung stehen, welche mit voreingestellten Fensterungswerten eine sofortige Visualisierung ermöglicht.
Über einen Slider und ein Textfeld soll der Nutzer die Möglichkeit haben die aktuelle Schicht auszuwählen.
Die Anzahl der verfügbaren Schichten soll ebenfalls in der GUI angezeigt werden.

\subsubsection{Vom Nutzer einstellbare Fensterung mit Zentrum und Breite (Windowing)}

Der Werte für die Fensterung des Datensatzes sollen vom Nutzer einstellbar sein.
Hierbei soll der Nutzer die Möglichkeit haben das Zentrum und die Breite des Fensters einzustellen.
Die beiden Werte sollen mit Textfeldern und mit Slidern einstellbar sein, wobei die Werte nur in gülten Bereich eingestellt werden können.
Die gültigen Bereich für das Zentrum der Fensterung soll zwischen -1024 und 3071 liegen.
Die gültigen Bereich für die Breite der Fensterung soll zwischen 1 und 4096 liegen.
Bei Änderungen der Werte soll die Visualisierung des Datensatzes in Echtzeit aktualisiert werden.

\subsubsection{Vom Nutzer einstellbarer Segmentierungs-Threshold}

Der Wert für den Segmentierungs-Threshold soll vom Nutzer einstellbar sein.
Hierbei soll der Nutzer die Möglichkeit haben den Threshold mit einem Textfeld und mit einem Slider einzustellen.
Der Wert soll nur in einem gültigen Bereich eingestellt werden können, welcher zwischen -1024 und 3071 liegt.
Über eine Checkbox soll der Nutzer die Möglichkeit haben den aktuellen Threshold in der 2D Ansicht des Datensatzes anzuzeigen.
Bei Änderungen des Wertes soll die Visualisierung des Datensatzes in Echtzeit aktualisiert werden.

\subsubsection{3D Darstellung des Datensatzes}

Mit einem Button soll der Nutzer die Möglichkeit haben eine 3D Darstellung des Datensatzes zu erstellen.
Da die berechnung der 3D Darstellung einige Zeit in Anspruch nehmen kann, soll dies nicht in Echtzeit erfolgen.
Die 3D Darstellung soll in einer eigenen Ansicht dargestellt werden, welche der Nutzer mit verschiedenen Buttons drehen kann.
Es soll einen eigenen Button geben um die 3D Darstellung zum Ursprung zurückzusetzen.

\subsection{Bohrung planen}

Das planen der Bohrung soll in einem eigenen Arbeitsschritt erfolgen.
Dieser soll erst begonnen werden können, wenn der Nutzer erfolgreich einen Datensatz eingelesen und eine 3D Darstellung erstellt hat.
Der Nutzer soll die Möglichkeit haben den Startpunkt der Bohrung auf der Knochenoberfläche zu setzen und den Endpunkt im Femurkopf zu setzen.
Ebenfalls soll die Bohrlänge in mm angezeigt werden.
Erst wenn der Nutzer erfolgreich den Start- und Endpunkt der Bohrung gesetzt hat, soll er den nächsten Arbeitsschritt beginnen können.

\subsubsection{Definition des Eintrittspunktes auf der Knochenoberfläche}

Der Eintrittspunkt der Bohrung soll auf der Knochenoberfläche definiert werden.
Hierbei soll der Nutzer die Möglichkeit haben den Eintrittspunkt durch klicken auf die Knochenoberfläche in der 3D Darstellung zu setzen.
Die Koordinaten des Eintrittspunktes sollen in Textfeldern angezeigt werden.
Ebenfalls soll der Nutzer die Möglichkeit haben die Koordinaten des Eintrittspunktes manuell einzugeben.
Die Koordinaten des Eintrittspunktes sollen in der 3D Darstellung durch ein Fadenkreuz dargestellt werden.
Bei jeder Änderung der Koordinaten soll ebenfalls die ausgewählte Schicht in der 2D Ansicht aktualisiert werden.
Mit einem Button soll der Nutzer die Möglichkeit haben den Eintrittspunkt zu bestätigen.
Wenn der Eintrittspunkt bestätigt wurde, soll dieser nicht mehr durch klicken in der 3D Darstellung oder durch manuelle Eingabe der Koordinaten verschoben werden können.

\subsubsection{Definition des Endpunktes im Femurkopf}

Der Endpunkt der Bohrung soll in der 2D Ansicht des Datensatzes definiert werden.
Hierbei soll der Nutzer die Möglichkeit haben den Endpunkt durch klicken in der 2D Ansicht zu setzen.
Die Koordinaten des Endpunktes sollen in Textfeldern angezeigt werden.
Ebenfalls soll der Nutzer die Möglichkeit haben die Koordinaten des Endpunktes manuell einzugeben.
Die Koordinaten des Endpunktes sollen in der 2D Darstellung durch ein Fadenkreuz dargestellt werden.
Bei jeder Änderung der z-Koordinate des Endpunktes soll die entsprechende Schicht in der 2D Ansicht darstellt werden.
Mit einem Button soll der Nutzer die Möglichkeit haben den Endpunkt zu bestätigen.
Wenn der Endpunkt bestätigt wurde, soll dieser nicht mehr durch klicken in der 2D Ansicht oder durch manuelle Eingabe der Koordinaten verschoben werden können.

\subsubsection{Anzeigen der Bohrlänge in mm}

Die Bohrlänge in mm soll dem Nutzer in einem Textfeld angezeigt werden.
Die Bohrlänge soll sich aus der Differenz des Start- und Endpunktes ergeben.
Ebenfalls soll der Nutzer die Möglichkeit haben die Bohrlänge manuell einzugeben.
In diesem Fall soll der Endpunkt entlang des Richtungsvektors des Bohrkanals verschoben werden und die 2D Ansicht aktualisiert werden.
Ist der Endpunkt bestätigt, soll auch die Bohrlänge nicht mehr manuell verändert werden können.

\subsection{Bohrung verifizieren}

Das verifizieren der Bohrung soll in einem eigenen Arbeitsschritt erfolgen.
Dieser soll erst begonnen werden können, wenn der Nutzer erfolgreich den Start- und Endpunkt der Bohrung gesetzt hat.
Der Nutzer soll die Möglichkeit haben die geplante Bohrung in verschiedenen Ansichten zu verifizieren.
Erst wenn der Nutzer erfolgreich die Bohrung verifiziert hat, soll er den nächsten Arbeitsschritt beginnen können.

\subsubsection{Darstellung einer rekonstruierten Schicht entlang der Bohrung}

Der Nutzer soll die Möglichkeit haben eine rekonstruierte Schicht entlang der Bohrung in der 2D Ansicht darzustellen.
Hier sollen verschiedene Ansichten zur Verfügung stehen, zwischen welchen der Nutzer wechseln kann.
Die verschiedenen Ansichten sollen mit Radio Buttons ausgewählt werden können.

\subsection{Bohrschablone berechnen}

Das berechnen und speichern der Bohrschablone soll in einem eigenen Arbeitsschritt erfolgen.
Dieser soll erst begonnen werden können, wenn der Nutzer erfolgreich die Bohrung verifiziert hat.
Der Nutzer soll die Möglichkeit haben die Bohrschablone zu planen und diese in einer .raw Datei zu speichern.
Ebenfalls soll der Nutzer die Möglichkeit haben die Koordinaten des Start- und Endpunktes der Bohrung sowie den Durchmesser des Bohrkanals in einer .txt Datei zu speichern.
Erst wenn der Nutzer erfolgreich die Bohrschablone berechnet und die Dateien gespeichert hat, ist das Programm erfolgreich abgeschlossen.

\subsubsection{Berechnen der Bohrschablone}

Die Bohrschablone soll in einer 3D Ansicht dargestellt werden.
Hierbei soll der Nutzer die Möglichkeit haben die Bohrschablone zu drehen.
Zur Berechnung der Bohrschablone soll der Nutzer die Möglichkeit haben den Durchmesser des Bohrkanals einzustellen.
Der Durchmesser des Bohrkanals soll in einem Textfeld einstellbar sein.
Die Bohrschablone soll sich in Echtzeit aktualisieren, wenn der Durchmesser des Bohrkanals verändert wird.

\subsubsection{Speichern der Bohrschablone als raw Datensatz}

Der Nutzer soll die Möglichkeit haben die Bohrschablone in einer .raw Datei zu speichern.
Hierbei soll der Nutzer die Möglichkeit haben den Speicherort und den Dateinamen der .raw Datei auszuwählen.
Ebenfalls soll der Nutzer die Möglichkeit haben die Koordinaten des Start- und Endpunktes der Bohrung sowie den Durchmesser des Bohrkanals in einer .txt Datei zu speichern.
Zum auswählen des Speicherortes und des Dateinamens soll ein Datei-Dialog zur Verfügung stehen.
Bei fehlern beim speichern der Datei soll eine Fehlermeldung ausgegeben werden, die den Nutzer auf das Problem hinweist.

\section{GUI Entwurf}

Im folgenden Abschnitt wird der Entwurf der grafischen Benutzeroberfläche (GUI) beschrieben.
Die GUI wird in vier Arbeitsschritte unterteilt, welche nacheinander abgearbeitet werden müssen und implementiert die im vorherigen Abschnitt beschriebenen funktionalen Anforderungen.

\subsection{Datensatz einladen und darstellen}

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{Gui/Folie1.png}
    \caption{GUI Entwurf: Datensatz einladen und darstellen}
    \label{fig:gui1}
\end{figure}

Beim Starten der Anwendung wird die GUI für den ersten Arbeitsschritt angezeigt. Als ersten Schritt muss der Nutzer einen Datensatz einladen und diesen visualisieren.
Mit dem Button Load Image siehe Abbildung \ref{fig:gui1} (1) kann der Nutzer einen Datensatz einladen und die visualisierung wird in der Ansicht (A) dargestellt.
Die Parameter für die Fensterung des Datensatzes können mit den Slidern und Feldern (2) eingestellt werden. Die Visualisierung wird in der Ansicht (A) wird hierbei kontinuierlich aktualisiert.

Ist der Nutzer mit der Fensterung zufrieden, kann er den Threshold (3) einstellen. Mit der Checkbox Show Threshold kann der aktuell eingestelle Threshold in der Ansicht (A) dargestellt werden.
Ist das korrekte Gewebe ausgewählt, kann mit dem Button Update 3D View (4) die 3D Darstellung erstellt oder nach Änderungen am Threshold aktualisiert werden.
Die 3D Darstellung wird in der Ansicht (B) dargestellt und kann mit den Buttons (5) gedreht werden.

Ist die 3D Darstellung erstellt ist der erste Arbeitsschritt abgeschlossen und der Nutzer kann den nächsten Arbeitsschritt beginnen. Signalisiert wird dies durch einen grünen Haken (i) neben dem Arbeitsschritt.

\subsubsection{Bohrung planen}

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{Gui/Folie2.png}
    \caption{GUI Entwurf: Bohrung planen}
    \label{fig:gui2}
\end{figure}

Der Startpunkt der Bohrung kann durch klicken auf die Gewebeoberfläche in der 3D Darstellung (B) gesetzt werden. Die Koordinaten des Startpunktes werden dann in den Feldern (2) angezeigt.
Hier können die Koordinaten auch manuell eingegeben werden. Die ausgewählten Koordinaten werden in der 3D Darstellung (B) durch ein Fadenkreuz dargestellt.
Ebenfalls wird die aktuelle Lage des Startpunktes in der Schichtansicht (A) dargestellt.
Ist der Startpunkt erfolgreich gesetzt, muss dieser durch den Button Select (3) bestätigt werden. Dies verhindert das versehentliche verschieben des Startpunktes durch klicken in der 3D Darstellung (B).

Anschließend kann der Nutzer den Endpunkt der Bohrung setzen. Hierfür in der Schichtansicht (A) ein Punkt mit der Maus ausgewählt (4) werden. Die Koordinaten des Endpunktes sowie die aktuelle Sicht kann mit den Feldern (5) bearbeitet werden.
Ebenfalls wird hier die aktuelle Länge des Bohrkanals angezeigt, welcher ebenfalls bearbeitet werden kann. Dies verschiebt den Endpunkt der Bohrung entlang des ausgewählten Richtungsvektors des Bohrkanals.
Ist der Endpunkt erfolgreich gesetzt, muss dieser durch den Button Select (6) bestätigt werden. Dies verhindert das versehentliche verschieben des Endpunktes durch klicken in der Ansicht (A).

Sind Start- und Endpunkt gesetzt, ist der aktuelle Arbeitsschritt abgeschlossen und der Nutzer kann den nächsten Arbeitsschritt beginnen. Signalisiert wird dies durch einen grünen Haken (ii) neben dem Arbeitsschritt.


\subsubsection{Bohrung verifizieren}

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{Gui/Folie3.png}
    \caption{GUI Entwurf: Bohrung verifizieren}
    \label{fig:gui3}
\end{figure}

Im Arbeitsschritt Bohrung verifizieren wird dem Nutzer die Möglichkeit gegeben die geplante Bohrung zu verifizieren.
Mit den Radio Buttons (1) kann der Nutzer zwischen verschiedenen Ansichten für die Darstellung (A) wählen. Die verschiedenen Ansichten sind an den Radio Buttons beschrieben.
Die Ansicht (B) kann analog mit den Radio Buttons (2) gewählt werden. Hier steht auch ebenfalls eine 3D Darstellung zur Verfügung.

Der Arbeitsschritt ist abgeschlossen, sobald die Verifikation geöffnet wurde. Signalisiert wird dies durch einen grünen Haken (iii) neben dem Arbeitsschritt.

\subsubsection{Bohrschablone berechnen}

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{Gui/Folie4.png}
    \caption{GUI Entwurf: Bohrschablone berechnen}
    \label{fig:gui4}
\end{figure}

Im letzten Arbeitsschritt Bohrschablone berechnen wird dem Nutzer die Möglichkeit gegeben die Bohrschablone zu planen.
Eine 3D Ansicht der Gewebeoberfläche auf der der Startpunkt der Bohrung gesetzt wurde wird in der Ansicht (A) dargestellt.
Die aktuelle Bohrschablone wird in der Ansicht (B) dargestellt. Hier kann der Nutzer die Bohrschablone ebenfalls drehen.

Der Durchmesser der Bohrkanals kann mit dem Textfeld (1) eingestellt werden. Die Bohrschablone wird in der Ansicht (B) kontinuierlich aktualisiert.

Ist der Nutzer mit der Bohrschablone zufrieden, kann er zunächst die Koordinaten des Start- und Endpunktes der Bohrung sowie den Durchmessers des Bohrkanals mit dem Button Save Position (2) in einer .txt Datei speichern.
Anschließend kann der Nutzer die Bohrschablone mit dem Button Save Template (3) in einer .raw Datei speichern.

Die erstellen Dateien müssen zwingen Überprüft werden. Sind beide Dateien korrekt erstellt, ist der Arbeitsschritt abgeschlossen und das Programm kann beendet werden. Signalisiert wird dies durch einen grünen Haken (iv) neben dem Arbeitsschritt.


\section{Bereitstellung/Terminplanung}
\begin{itemize}
    \item 18.10.2023: Fensterung des Datensatzes
    \item 08.11.2023: Segmentierungs-Threshold und 3D Darstellung
    \item 06.12.2023: Erstellung des Pflichtenhefts
    \item 13.12.2023: Koordinatensystem der 3D Darstellung
    \item 20.12.2023: Risikoanalyse
    \item 17.01.2024: Planung der Bohrung und verifikation des Bohrkanals
    \item 31.01.2024: Erstellung der Bohrschablone
    \item 06.02.2024: Abgabe der Software
    \item 07.02.2024: Präsentation der Software
\end{itemize}

\section{Hinweise und Anmerkungen}
\subsection{Mitgeltende Unterlagen}
\begin{itemize}
    \item Folien der Vorlesung SWEIDMT
    \item Übungsblätter der Vorlesung SWEIDMT
    \item IEC 62304 - Medizinprodukte - Software-Lebenszyklusprozesse
    \item ISO 14971 - Medizinprodukte - Anwendung des Risikomanagements auf Medizinprodukte
    \item ISO 13485 - Medizinprodukte - Qualitätsmanagementsysteme
\end{itemize}


\clearpage
\end{document}