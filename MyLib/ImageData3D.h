/**
 * @file
 * @author Tim Heß
 * @date 01.02.2024
 */

#ifndef SWE_IMAGEDATA3D_H
#define SWE_IMAGEDATA3D_H

/**
 * @brief The ImageData3D class is used to store square 3D image data with a variable number of layers.
 */
class ImageData3D {
public:
    short *data; //!< The actual image data.
    int length; //!< The length of one side of square layer.
    int layers; //!< The number of layers of the 3D image.

    /**
     * @brief Default constructor of the ImageData3D class.
     * The data pointer is set to nullptr, the length and layers are set to 0.
     */
    ImageData3D() : data(nullptr), length(0), layers(0) {}

    /**
     * @brief Constructor of the ImageData3D class.
     * The data pointer is set to a new array of length*length*layers and the corresponding length and layers are set.
     * @param length The length of one side of the square layer.
     * @param layers The number of layers of the 3D image.
     */
    ImageData3D(int length, int layers) : data(new short[length*length*layers] ), length(length), layers(layers) {}

    /**
     * @brief Destructor of the ImageData3D class.
     * The data is deleted.
     */
    ~ImageData3D() { delete[] data; }

    /**
     * @brief Check if the image data is loaded.
     * @return True if the image data is loaded, false otherwise.
     */
    bool Loaded() {
        return data != nullptr;
    }

    /**
     * @brief Set the length and layers of the image data.
     * The data is deleted and set to a new array of length*length*layers and the corresponding length and layers are set.
     * @param length The length of one side of the square layer.
     * @param layers The number of layers of the 3D image.
     */
    void Set(int setLength, int setLayers) {
        delete[] this->data;
        this->data = new short [setLength * setLength * setLayers];
        this->length = setLength;
        this->layers = setLayers;
    }

    /**
     * @brief Scale the image data.
     * The data is scaled by the given factor.
     * @param factor The factor to scale the image data by. Only integer factors are supported.
     */
    void Scale(int factor) {
        short *newData = new short [length * length * layers * factor * factor * factor];
        for (int x = 0; x < length; x++) {
            for (int y = 0; y < length; y++) {
                for (int z = 0; z < layers; z++) {
                    for (int fx = 0; fx < factor; fx++) {
                        for (int fy = 0; fy < factor; fy++) {
                            for (int fz = 0; fz < factor; fz++) {
                                newData[(x * factor + fx) + (y * factor + fy) * length * factor + (z * factor + fz) * length * factor * length * factor] = data[x + y * length + z * length * length];
                            }
                        }
                    }
                }
            }
        }
        delete[] this->data;
        this->data = newData;
        this->length *= factor;
        this->layers *= factor;
    }

    /**
     * @brief Increase the size of the image data.
     * The data is increased to the given length and layers. The new data is filled with the given empty value. The old data is placed in the center.
     * @param newLength The new length of one side of the square layer.
     * @param newLayers The new number of layers of the 3D image.
     * @param defaultValue The value to fill the new data with.
     */
    void IncreaseSize(int newLength, int newLayers, int defaultValue = -1024) {
        auto *newData = new short [newLength * newLength * newLayers];
        std::fill(newData, newData + newLength * newLength * newLayers, defaultValue);

        // Place old data in the center
        for (int x = 0; x < this->length; x++) {
            for (int y = 0; y < this->length; y++) {
                for (int z = 0; z < this->layers; z++) {
                    newData[(x + (newLength - this->length) / 2) + (y + (newLength - this->length) / 2) * newLength + (z + (newLayers - this->layers) / 2) * newLength * newLength] = data[x + y * length + z * length * length];
                }
            }
        }

        delete[] this->data;
        this->data = newData;
        this->length = newLength;
        this->layers = newLayers;
    }
};


#endif //SWE_IMAGEDATA3D_H
