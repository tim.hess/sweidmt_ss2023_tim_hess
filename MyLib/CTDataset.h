/**
 * @file
 * @author Tim Heß
 * @date 22.11.2023
 */

#if defined(MYLIB_EXPORT)
# define MYLIB_EXPORT_IMPORT Q_DECL_EXPORT
#else
# define MYLIB_EXPORT_IMPORT Q_DECL_IMPORT
#endif

#include <QString>
#include "Eigen/Core"
#include "Eigen/Dense"
#include "ImageData2D.h"
#include "ImageData3D.h"
#include "WindowingConfig.h"
#include "ReturnCode.h"


/**
 * @brief The CTDataset class is used to load, store and manipulate data from a CT scan.
 */
class MYLIB_EXPORT_IMPORT CTDataset
{
private:
    ImageData3D *m_pImageData; //!< ct image data
    ImageData3D *m_pRotatedImageData; //!< rotated ct image data
    ImageData2D *m_pDepthBuffer; //!< depth buffer
    ImageData2D *m_pShadedDepthBuffer; //!< shaded depth buffer
    ImageData3D *m_pTemplate; //!< template data

public:
    /// Constructor of the CTDataset class.
    CTDataset();
    /// Destructor of the CTDataset class.
    ~CTDataset();


    /// Returns the ct image data.
    ImageData3D*  data();
    /// Returns the rotated ct image data.
    ImageData3D* rotatedData();
    /// Returns the depth buffer.
    ImageData2D* depthBuffer();
    /// Returns the shaded depth buffer.
    ImageData2D* shadedDepthBuffer();
    /// Returns the template.
    ImageData3D* templateData();

    /// Applies a windowing to the given HU value.
    static ReturnCode windowing(int HU_value, WindowingConfig config, int &greyValue);

    /// Loads the ct image data from the given file path.
    static ReturnCode load(const QString &imagePath, int size, ImageData3D *imageData);

    /// Rotates the 3d image data by the given rotation matrix.
    static void rotateData(const ImageData3D *originalData, const Eigen::Matrix3d* rotation, ImageData3D *rotatedData, int defaultValue = -1024);

    /// Transforms the given point by the given rotation matrix and the given image data.
    static Eigen::Vector3i
    transformPoint(const Eigen::Vector3i &point, const Eigen::Matrix3d* rotation, const ImageData3D *imageData);

    /// Reverses the transformation of the given point by the given rotation matrix and the given image data.
    static Eigen::Vector3i
    reverseTransformPoint(const Eigen::Vector3i &point, const Eigen::Matrix3d* rotation, const ImageData3D *imageData);

    /// Renders a slice view of the given 2d image data.
    static ReturnCode renderSliceView(QImage *image, const ImageData2D* data, const WindowingConfig windowingConfig);

    /// Renders a slice view of the given 3d image data.
    static ReturnCode
    renderSliceView(QImage *image, const ImageData3D* data, int layer, const WindowingConfig windowingConfig);

    /// Renders a slice view of the given data array.
    static ReturnCode
    renderSliceView(QImage *image, const short *data, int length, const WindowingConfig windowingConfig,
                    int offset);

    /// Calculates the depth buffer of the given 3d image data.
    static ReturnCode calculateDepthBuffer(const ImageData3D *inputData, int threshold, ImageData2D *depthBuffer);

    /// Calculates the shaded depth buffer of the given depth buffer.
    static ReturnCode calculateShadedDepthBuffer(const ImageData2D *depthBuffer, ImageData2D *shadedDepthBuffer, int defaultValue = 255);

    /// Returns a slice of the given 3d image data at the given position with the given x and y direction.
    static ReturnCode getSlice(const ImageData3D *inputData, const Eigen::Vector3i &pos, const Eigen::Vector3d &xdir,
                        const Eigen::Vector3d &ydir, ImageData2D *slice, int defaultValue = -1024);

    /// Creates a template of the given 3d image data with the given threshold, diameter, start and end point.
    static ReturnCode
    createTemplate(const ImageData3D *inputData, int threshold, int diameter, const Eigen::Vector3d& start, const Eigen::Vector3d& end,
                   ImageData3D *templateData);

    /// Applies the given rotation to the given rotation matrix.
    static void applyRotation(int rotationIndex, Eigen::Matrix3d *rotation);

    /// Renders the given 2d image data as a shaded depth buffer.
    static ReturnCode
    renderShadedDepthBuffer(QImage *image, const ImageData2D *shadedDepthBuffer);

    /// Converts the given original point to plane coordinates with the given x and y direction, position and slice.
    static Eigen::Vector3d convertToPlaneCoordinates(const Eigen::Vector3d &originalPoint, const Eigen::Vector3d &xdir,
                                                     const Eigen::Vector3d &ydir, const Eigen::Vector3d &pos, const ImageData2D* slice);
};



