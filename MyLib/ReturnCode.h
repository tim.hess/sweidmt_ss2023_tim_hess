/**
 * @file
 * @author Tim Heß
 * @date 02.02.2024
 */

#ifndef SWE_RETURNCODE_H
#define SWE_RETURNCODE_H

/**
 * @enum ReturnCode
 * @brief The ReturnCode enum is used to indicate the return status of a function of the MyLib library.
 */
enum class ReturnCode {
    OK, //!< No error occurred.
    HU_OUT_OF_RANGE, //!< The HU value is out of range.
    CENTER_OUT_OF_RANGE, //!< The center value is out of range.
    WIDTH_OUT_OF_RANGE, //!< The length value is out of range.
    ERROR_OPEN_FILE, //!< The file could not be opened.
    ERROR_READ_FILE, //!< The file could not be read.
    ERROR_UNSUPPORTED_FILE, //!< The file is not supported.
    ERROR_LAYER_OUT_OF_RANGE, //!< The layer is out of range.
};


#endif //SWE_RETURNCODE_H
