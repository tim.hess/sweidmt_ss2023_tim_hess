/**
 * @file
 * @author Tim Heß
 * @date 22.11.2023
 */

#include <QFile>
#include "CTDataset.h"
#include <cmath>
#include <QDebug>
#include <future>
#include <QLabel>

#define NUMBER_OF_THREADS 8

/// Supported image size for the CT scan.
const int IMAGE_SIZE = 512;

/**
 * Default constructor of the CTDataset class.
 * The image data, the rotated image data, the depth buffer, the shaded depth buffer and the template are initialized.
 */
CTDataset::CTDataset() {
    m_pImageData = new ImageData3D();
    m_pRotatedImageData = new ImageData3D();
    m_pDepthBuffer = new ImageData2D();
    m_pShadedDepthBuffer = new ImageData2D();
    m_pTemplate = new ImageData3D();
}

/**
 * Destructor of the CTDataset class.
 * The image data, the rotated image data, the depth buffer, the shaded depth buffer and the template are deleted.
 */
CTDataset::~CTDataset() {
    delete m_pImageData;
    delete m_pRotatedImageData;
    delete m_pDepthBuffer;
    delete m_pShadedDepthBuffer;
    delete m_pTemplate;
}

/**
 * Loads the image data from the given path checking if the file size is valid for a CT scan.
 *
 * The image data will be stored in the passed imageData object. Layer size and number of layers will be set accordingly.
 *
 * @param imagePath The path to the image data.
 * @param size The expected length of one side of the square layer.
 * @param imageData The image data to store the loaded data.
 * @return The status of the function.
 * @retval ReturnCode::OK The image data was loaded successfully.
 * @retval ReturnCode::ERROR_OPEN_FILE The file could not be opened.
 * @retval ReturnCode::ERROR_UNSUPPORTED_FILE The file size is not valid for a CT scan.
 * @retval ReturnCode::ERROR_READ_FILE The file could not be read.
 */
ReturnCode CTDataset::load(const QString& imagePath, int size, ImageData3D* imageData) {
    // Open the file
    QFile dataFile(imagePath);
    bool bFileOpen = dataFile.open(QIODevice::ReadOnly);
    if (!bFileOpen) {
        return ReturnCode::ERROR_OPEN_FILE;
    }

    // Check file size
    qint64 iFileSize = dataFile.size();
    if (iFileSize % (size*size*2) != 0 || iFileSize < (size*size*2)) {
        return ReturnCode::ERROR_UNSUPPORTED_FILE;
    }

    // Read file
    qint64 iNumberBytesRead;
    int layers = (int) (iFileSize / (size*size*2));
    imageData->Set(size, layers);
    iNumberBytesRead = dataFile.read((char *) imageData->data, iFileSize);
    if (iFileSize != iNumberBytesRead) {
        return ReturnCode::ERROR_READ_FILE;
    }

    // Close file
    dataFile.close();
    return ReturnCode::OK;
}


/**
 * Applies a rotation based on the given rotation index to the passed rotation matrix.
 *
 * The following rotations are supported:
 * - 0: 45° around the x-axis
 * - 1: -45° around the x-axis
 * - 2: 45° around the y-axis
 * - 3: -45° around the y-axis
 * - 4: 45° around the z-axis
 * - 5: -45° around the z-axis
 * - -1: Reset the rotation matrix to the identity matrix
 *
 * @param rotationIndex The index of the rotation.
 * @param rotation The rotation of the dataset.
 */
void CTDataset::applyRotation(int rotationIndex, Eigen::Matrix3d* rotation) {
    Eigen::AngleAxisd angle;
    switch (rotationIndex) {
        case 0:
            angle = Eigen::AngleAxisd(-45.0/180.0*M_PI, Eigen::Vector3d::UnitX());
            break;
        case 1:
            angle = Eigen::AngleAxisd(45.0/180.0*M_PI, Eigen::Vector3d::UnitX());
            break;
        case 2:
            angle = Eigen::AngleAxisd(-45.0/180.0*M_PI, Eigen::Vector3d::UnitY());
            break;
        case 3:
            angle = Eigen::AngleAxisd(45.0/180.0*M_PI, Eigen::Vector3d::UnitY());
            break;
        case 4:
            angle = Eigen::AngleAxisd(45.0/180.0*M_PI, Eigen::Vector3d::UnitZ());
            break;
        case 5:
            angle = Eigen::AngleAxisd(-45.0/180.0*M_PI, Eigen::Vector3d::UnitZ());
            break;
        case -1:
            rotation->matrix() = Eigen::Matrix3d::Identity();
            return;
        default:
            return;
    }

    // Apply rotation to the given rotation matrix
    rotation->matrix() = angle.toRotationMatrix() * rotation->matrix();
}

/**
 * Rotates the given 3D image data by the given rotation matrix.
 *
 * The rotated data will be stored in the passed rotatedData object.
 * If the image data is not cubic the data will also be translated to avoid information loss.
 *
 * @param originalData The original image data.
 * @param rotation The rotation of the dataset.
 * @param rotatedData The rotated image data.
 * @param defaultValue The value to fill the new data with before rotation.
 */
void CTDataset::rotateData(const ImageData3D* originalData, const Eigen::Matrix3d* rotation, ImageData3D* rotatedData, int defaultValue) {

    // Create buffer to store the rotated data and fill it with the empty value
    int size = originalData->length * originalData->length * originalData->layers;
    rotatedData->Set(originalData->length, originalData->layers);
    std::fill_n(rotatedData->data, size, defaultValue);

    // Calculate the center and the translation
    Eigen::Vector3d Center = Eigen::Vector3d(originalData->length / 2.0, originalData->length / 2.0, originalData->layers / 2.0);
    Eigen::Vector3d Trans = Eigen::Vector3d(0, 0, 0);
    if (originalData->length != originalData->layers) {
        Trans = Eigen::Vector3d(0, 0, originalData->layers / 2.0);
    }

    // Calculate the inverse of the rotation matrix
    Eigen::Matrix3d rotation_inv = rotation->transpose();

    // Define the lambda function to be executed by the threads
    auto f = [Center, Trans, originalData, rotation_inv, rotatedData](int threadIndex) {
        int width = originalData->length;
        int height = originalData->length;
        int layers = originalData->layers;
        short *origData = originalData->data;
        short *rotData = rotatedData->data;
        int numThreads = NUMBER_OF_THREADS;
        Eigen::Vector3d P_rot, P_orig;
        Eigen::Vector3i P_orig_i;
        int index, index_rot;

        for (int x = (width / numThreads) * threadIndex; x < (width / numThreads) * (threadIndex+1); x++) {
            P_rot.x() = x;
            for (int y = 0; y < height; y++) {
                P_rot.y() = y;
                for (int z = 0; z < layers; z++) {
                    P_rot.z() = z;

                    // Rotate and translate the point
                    P_orig = rotation_inv * (P_rot - Center - Trans) + Center;
                    P_orig_i = P_orig.cast<int>();

                    // Check if the point is in range
                    if (P_orig_i.x() < 0 || P_orig_i.x() >= width ||
                        P_orig_i.y() < 0 || P_orig_i.y() >= height ||
                        P_orig_i.z() < 0 ||P_orig_i.z() >= layers) {
                        continue;
                    }

                    // Copy the value to the rotated data
                    index = x + (y * width) + (z * width * height);
                    index_rot = P_orig_i.x() + (P_orig_i.y() * width) + (P_orig_i.z() * width * height);
                    rotData[index] = origData[index_rot];
                }
            }
        }
    };

    // Create threads and start them
    std::vector<std::thread> threads;
    for (int i = 0; i < NUMBER_OF_THREADS; i++) {
        threads.emplace_back(f, i);
    }
    for (auto& th : threads)
        th.join();
}

/**
 * Transforms the given point by the given rotation matrix inside the given image data.
 *
 * The point will be transformed by the given rotation matrix and the center of the image data.
 * If the image data is not cubic the point will also be translated to avoid information loss.
 *
 * @param point The point to transform.
 * @param rotation The rotation of the dataset.
 * @param imageData The image data.
 * @return The transformed point.
 */
Eigen::Vector3i CTDataset::transformPoint(const Eigen::Vector3i &point, const Eigen::Matrix3d* rotation, const ImageData3D* imageData) {
    // Calculate the center and the translation
    Eigen::Vector3d Center = Eigen::Vector3d(imageData->length / 2.0, imageData->length / 2.0, imageData->layers / 2.0);
    Eigen::Vector3d Trans = Eigen::Vector3d(0, 0, 0);
    if (imageData->length != imageData->layers) {
        Trans = Eigen::Vector3d(0, 0, imageData->layers / 2.0);
    }

    // Transform the point
    Eigen::Vector3d point_d = rotation->matrix() * (point.cast<double>() - Center) + Center + Trans ;
    return point_d.cast<int>();
}

/**
 * Reverses the transformation of the given point by the given rotation matrix inside the given image data.
 *
 * The point will be transformed by the transpose of the given rotation matrix and the center of the image data.
 * If the image data is not cubic the point will also be translated to avoid information loss.
 *
 * @param point The point to transform.
 * @param rotation The rotation of the dataset.
 * @param imageData The image data.
 * @return The transformed point.
 */
Eigen::Vector3i CTDataset::reverseTransformPoint(const Eigen::Vector3i &point, const Eigen::Matrix3d* rotation, const ImageData3D* imageData) {
    // Calculate the center and the translation
    Eigen::Vector3d Center = Eigen::Vector3d(imageData->length / 2.0, imageData->length / 2.0, imageData->layers / 2.0);
    Eigen::Vector3d Trans = Eigen::Vector3d(0, 0, 0);
    if (imageData->length != imageData->layers) {
        Trans = Eigen::Vector3d(0, 0, imageData->layers / 2.0);
    }

    // Transform the point
    Eigen::Vector3d point_d = rotation->transpose() * (point.cast<double>() - Center - Trans) + Center;
    return point_d.cast<int>();
}


/**
 * Calculates the grey value for the given HU value using a windowing function.
 * The following formula is used:
 *
 * greyValue = 0 + ((HU_value - x_1) / (x_2 - x_1)) * (255)
 * where
 * - x_1 = center - length / 2
 * - x_2 = center + length / 2
 *
 * If the HU value is smaller than x_1 the grey value will be 0.
 * If the HU value is greater than x_2 the grey value will be 255.
 * The maximum and minimum values for the HU value, the center and the length are:
 * - HU value: -1024 to 3071
 * - center: -1024 to 3071
 * - length: 1 to 4095
 *
 * @param HU_value The HU value.
 * @param config The windowing configuration to be used.
 * @param greyValue The calculated grey value.
 * @return The status of the function.
 * @retval ReturnCode::OK The calculation was successful.
 * @retval ReturnCode::CENTER_OUT_OF_RANGE The center value is out of range.
 * @retval ReturnCode::WIDTH_OUT_OF_RANGE The length value is out of range.
 * @retval ReturnCode::HU_OUT_OF_RANGE The HU value is out of range.
 */
ReturnCode CTDataset::windowing(const int HU_value, const WindowingConfig config, int &greyValue) {

    // Check if values are in range
    if (config.center < -1024 || config.center > 3071) return ReturnCode::CENTER_OUT_OF_RANGE;
    if (config.width < 1 || config.width > 4095) return ReturnCode::WIDTH_OUT_OF_RANGE;
    if (HU_value < -1024 || HU_value > 3071) return ReturnCode::HU_OUT_OF_RANGE;

    // Check if HU value is out of range
    if (HU_value <= config.center - config.width / 2) {
        greyValue = 0;
        return ReturnCode::OK;
    }
    if (HU_value >= config.center + config.width / 2) {
        greyValue = 255;
        return ReturnCode::OK;
    }

    // Calculate grey value
    greyValue = (int) std::roundf(0.0f + (((float)(HU_value) - (float)(config.center) + (float)(config.width)/2.0f) / (float)(config.width)) * 255.0f);
    return ReturnCode::OK;
}

/**
 * Renders a slice view of the given 2D image data.
 *
 * The slice view will be stored in the passed image object.
 *
 * @param image The image to store the slice view.
 * @param data The 2D image data.
 * @param windowingConfig The windowing configuration to be used.
 * @return The status of the function.
 * @retval ReturnCode::OK The slice view was rendered successfully.
 * @retval Any error code from the function CTDataset::windowing().
 */
ReturnCode CTDataset::renderSliceView(QImage *image, const ImageData2D* data, const WindowingConfig windowingConfig) {
    return CTDataset::renderSliceView(image, data->data, data->length, windowingConfig, 0);
}

/**
 * Renders a slice view of the given 3D image data.
 *
 * The slice view will be stored in the passed image object.
 *
 * @param image The image to store the slice view.
 * @param data The 3D image data.
 * @param layer The layer to render.
 * @param windowingConfig The windowing configuration to be used.
 * @return The status of the function.
 * @retval ReturnCode::OK The slice view was rendered successfully.
 * @retval ReturnCode::ERROR_LAYER_OUT_OF_RANGE The layer is out of range.
 * @retval Any error code from the function CTDataset::renderSliceView().
 */
ReturnCode CTDataset::renderSliceView(QImage *image, const ImageData3D* data, const int layer, const WindowingConfig windowingConfig) {
    if (layer < 0 || layer >= data->layers) {
        return ReturnCode::ERROR_LAYER_OUT_OF_RANGE;
    }
    int offset = layer * data->length * data->length;
    return CTDataset::renderSliceView(image, data->data, data->length, windowingConfig, offset);
}

/**
 * Renders a slice view of the given data array.
 *
 * The slice view will be stored in the passed image object.
 *
 * @param image The image to store the slice view.
 * @param data The data array.
 * @param length The length of the data array.
 * @param windowingConfig The windowing configuration to be used.
 * @param offset The offset to the data array.
 * @return The status of the function.
 * @retval ReturnCode::OK The slice view was rendered successfully.
 * @retval Any error code from the function CTDataset::windowing().
 */
ReturnCode CTDataset::renderSliceView(QImage *image, const short* data, const int length, const WindowingConfig windowingConfig, int offset = 0) {
    int greyValue;
    unsigned int qRGB;
    ReturnCode rc;

    for (int index=0; index<(length * length); index++) {

        // Apply windowing
        rc = CTDataset::windowing(data[index + offset], windowingConfig, greyValue);
        if (rc != ReturnCode::OK) {
            return rc;
        }
        // Set pixel and apply threshold
        qRGB = qRgb(greyValue, greyValue, greyValue);
        if (windowingConfig.showThreshold && data[index + offset] >= windowingConfig.threshold) {
            qRGB = qRgb(255, 0, 0);
        }
        image->setPixel(index%length, index / length, qRGB);
    }
    return ReturnCode::OK;
}

/**
 * Renders the depth buffer to the given image object.
 *
 * The result will be stored in passed image object.
 *
 * @param inputData The image data.
 * @return The status of the function.
 * @retval ReturnCode::OK The depth buffer was calculated successfully.
 */
ReturnCode CTDataset::renderShadedDepthBuffer(QImage *image, const ImageData2D* shadedDepthBuffer) {
    int greyValue;
    for (int index=0; index< shadedDepthBuffer->length * shadedDepthBuffer->length; index++) {
        greyValue = shadedDepthBuffer->data[index];
        image->setPixel(index%shadedDepthBuffer->length, index / shadedDepthBuffer->length, qRgb(greyValue, greyValue, greyValue));
    }
    return ReturnCode::OK;
}

/**
 * Calculates the depth buffer for the given image data.
 *
 * The depth buffer will be stored in the passed depthBuffer object.
 *
 * The depth buffer is calculated by iterating over all pixels and all layers and checking if the grey value is greater than the threshold.
 * If the grey value is greater than the threshold the layer index will be stored in the depth buffer.
 *
 * @param inputData The image data.
 * @param threshold The threshold to use.
 * @param depthBuffer The depth buffer to store the result.
 * @return The status of the function.
 * @retval ReturnCode::OK The depth buffer was calculated successfully.
 */
ReturnCode CTDataset::calculateDepthBuffer(const ImageData3D* inputData, const int threshold, ImageData2D* depthBuffer) {
    // Prepare depth buffer
    depthBuffer->Set(inputData->length);

    // Calculate depth buffer
    for (int index=0; index< depthBuffer->length * depthBuffer->length; index++) {
        depthBuffer->data[index] = (short) inputData->layers;
        for (int layer=0; layer<inputData->layers; layer++) {
            if (inputData->data[index + (depthBuffer->length * depthBuffer->length * layer)] >= threshold) {
                depthBuffer->data[index] = (short) layer;
                break;
            }
        }
    }
    return ReturnCode::OK;
}

/**
 * Renders the depth buffer for the given depth buffer.
 *
 * The result will be stored in the passed shadedDepthBuffer object.
 *
 * The shaded depth buffer is calculated by iterating over all pixels and calculating the gradient of the depth buffer using a step size of 2.
 * The outer pixels will be set to 0 as they do not have any neighbors to calculate the gradient.
 * The following formula is used:
 *
 * shadedDepthBuffer = 255 * (sx*sy) / sqrt((sy*Tx)^2 + (sx*Ty)^2 + (sx*sy)^2)
 * where
 *
 * - Tx = gradient in x direction with a step size of sx
 * - Ty = gradient in y direction with a step size of sy
 * - sx = step size in x direction with a value of 2
 * - sy = step size in y direction with a value of 2
 *
 * @param depthBuffer The depth buffer to render.
 * @param shadedDepthBuffer The shaded depth buffer to store the result.
 * @param defaultValue The value to set for the outer pixels.
 * @return The status of the function.
 * @retval ReturnCode::OK The shaded depth buffer was calculated successfully.
 */
ReturnCode CTDataset::calculateShadedDepthBuffer(const ImageData2D* depthBuffer, ImageData2D* shadedDepthBuffer, int defaultValue) {
    // Prepare shaded depth buffer
    shadedDepthBuffer->Set(depthBuffer->length);

    // Render depth buffer
    int Tx, Ty;
    for (int index=0; index< depthBuffer->length * depthBuffer->length; index++) {
        if (index % depthBuffer->length == 0 || index % depthBuffer->length == depthBuffer->length - 1 || index / depthBuffer->length == 0 || index / depthBuffer->length == depthBuffer->length - 1) {
            shadedDepthBuffer->data[index] = (short) defaultValue;
            continue;
        }

        // Calculate Tx, Ty and shaded depth buffer
        Tx = depthBuffer->data[index-1] - depthBuffer->data[index+1];
        Ty = depthBuffer->data[index-depthBuffer->length] - depthBuffer->data[index + depthBuffer->length];
        shadedDepthBuffer->data[index] = (short) (255 * (2 * 2) / sqrt(pow(2 * Tx, 2) + pow(2 * Ty, 2) + pow(2 * 2, 2)));
    }
    return ReturnCode::OK;
}

/**
 * Calculates the slice of the given 3D image data at the given position with the given x and y direction.
 *
 * The slice will be stored in the passed slice object.
 *
 * The slice is generated by spanning a plane through the 3D image data at the given position with the given x and y direction.
 * The plane is then sampled around the center position with a side length set by the length of the given slice.
 * If the sampled position is out of range the value will be set to -1024.
 *
 * @param inputData The 3D image data.
 * @param pos The center position of the slice.
 * @param xdir The x direction of the slice.
 * @param ydir The y direction of the slice.
 * @param slice The slice to store the result.
 * @param defaultValue The value to fill slices with as a default value.
 * @return The status of the function.
 * @retval ReturnCode::OK The slice was calculated successfully.
 */
ReturnCode CTDataset::getSlice(const ImageData3D* inputData, const Eigen::Vector3i& pos, const Eigen::Vector3d &xdir, const Eigen::Vector3d &ydir, ImageData2D* slice, int defaultValue) {
    // Make sure that xdir and ydir are normalized
    Eigen::Vector3d xdir_normalized = xdir.normalized();
    Eigen::Vector3d ydir_normalized = ydir.normalized();

    for (int x=0; x<slice->length; x++) {
        for (int y=0; y<slice->length; y++) {
            // Get coordinates of the point in the plane
            Eigen::Vector3d P = pos.cast<double>() + xdir_normalized * (x - slice->length / 2) + ydir_normalized * (y - slice->length / 2);
            Eigen::Vector3i P_i = P.cast<int>();

            // Check if the point is in range, if not set the value to the empty value otherwise set the value to the value of the 3D image data
            if (P_i.x() < 0 || P_i.x() >= inputData->length || P_i.y() < 0 || P_i.y() >= inputData->length || P_i.z() < 0 || P_i.z() >= inputData->layers) {
                slice->data[x + (y * slice->length)] = (short) defaultValue;
            } else {
                slice->data[x + (y * slice->length)] = inputData->data[P_i.x() + (P_i.y() * inputData->length) + (P_i.z() * inputData->length * inputData->length)];
            }
        }
    }
    return ReturnCode::OK;
}

/**
 * Converts the given 3D point to the plane coordinates of the given plane by the given x and y direction, the given center position and the given slice.
 *
 * @param originalPoint point to convert
 * @param xdir x direction of the plane
 * @param ydir y direction of the plane
 * @param pos center position of the plane
 * @param slice slice to convert to. The length of the slice is used to calculate the plane coordinates.
 * @return The converted point as a 3D vector with the z coordinate set to 0.
 */
Eigen::Vector3d CTDataset::convertToPlaneCoordinates(const Eigen::Vector3d& originalPoint, const Eigen::Vector3d& xdir, const Eigen::Vector3d& ydir, const Eigen::Vector3d& pos, const ImageData2D* slice) {
    // Project the point to the plane
    Eigen::Vector3d normal = xdir.cross(ydir).normalized();
    Eigen::Vector3d projectedPoint = originalPoint - pos - (originalPoint.dot(normal) * normal);
    // Calculate the coordinates of the projected point in the plane coordinate system
    double xCoord = projectedPoint.dot(xdir) + (double(slice->length) / 2);
    double yCoord = projectedPoint.dot(ydir) + (double(slice->length) / 2);
    // Make sure that the coordinates are in range
    xCoord = std::max(0.0, std::min(double(slice->length), xCoord));
    yCoord = std::max(0.0, std::min(double(slice->length), yCoord));

    return {xCoord, yCoord, 0.0};
}

/**
 * Creates a template of the given 3D image data with the given threshold, diameter, start and end point.
 *
 * The template will be stored in the passed templateData object and has a side length of 40.
 *
 * The template is generated by creating a square with a side length of 40 and a depth of 40.
 * The set borehole with the given diameter is removed from the square.
 * The template is then rotated and translated to the start point of the borehole in such a way that the start of the borehole 10 layers deep and at the center of the square.
 * Then every voxel in the template is mapped to the 3D image data and the voxel is removed if the value of the 3D image data is greater than the threshold.
 *
 * @param inputData The 3D image data.
 * @param threshold The threshold to use.
 * @param diameter The diameter to use.
 * @param start The start point of the borehole.
 * @param end The end point of the borehole.
 * @param templateData The template to store the result.
 * @return The status of the function.
 * @retval ReturnCode::OK The template was created successfully.
 */
ReturnCode CTDataset::createTemplate(const ImageData3D* inputData, const int threshold, const int diameter, const Eigen::Vector3d& start, const Eigen::Vector3d& end, ImageData3D* templateData) {
    // Prepare template buffer
    int size = 40 * 40 * 40;
    templateData->Set(40, 40);
    std::fill_n(templateData->data, size, 1);

    // Calculate the direction and the angles of the borehole
    Eigen::Vector3d dir = (end - start).normalized();
    double angleZ = acos(dir.dot(Eigen::Vector3d(0, 0, 1)));
    double angleY = acos(dir.dot(Eigen::Vector3d(0, 1, 0)));

    // Calculate the global rotation from the template coordinate system to the global coordinate system
    Eigen::Matrix3d globalRotation = Eigen::Matrix3d::Identity() *
                                     Eigen::AngleAxisd(-90.0/180.0*M_PI, Eigen::Vector3d::UnitY()).toRotationMatrix() *
                                     Eigen::AngleAxisd(90.0/180.0*M_PI, Eigen::Vector3d::UnitZ()).toRotationMatrix();

    // Calculate the rotation for the borehole angles
    Eigen::Matrix3d boreholeRotation = Eigen::AngleAxisd(90-angleY, Eigen::Vector3d::UnitY()).toRotationMatrix() *
                                       Eigen::AngleAxisd(angleZ, Eigen::Vector3d::UnitZ()).toRotationMatrix();

    // Start point of the template in template coordinate system
    Eigen::Vector3d startTemplate = Eigen::Vector3i(20, 20, 10).cast<double>();
    // Center point of the template in template coordinate system
    Eigen::Vector3d centerTemplate = Eigen::Vector3i(20, 20, 20).cast<double>();
    // Start point of the template in global coordinate system
    Eigen::Vector3d startTemplateGlobal = globalRotation * (startTemplate - centerTemplate) + centerTemplate;
    // Translatation of the start point of the template to the borehole start
    Eigen::Vector3d translate = start - startTemplateGlobal;

    // Go over every point in the template and map it to the input data
    Eigen::Vector3i P, P_transformed;
    Eigen::Vector3d dP_transformed, boreholeCenter, rotatePoint;
    int indexInput;
    for (int x=0; x < 40; x++) {
        for (int y=0; y < 40; y++) {
            for (int z=0; z < 40; z++) {

                // Borhole center in template coordinate system
                boreholeCenter = Eigen::Vector3i(20, 20, z).cast<double>();
                // Current point in the template coordinate system
                P = Eigen::Vector3i(x, y, z);
                // Translate point into global coordinate system
                dP_transformed = globalRotation * (P.cast<double>() - centerTemplate) + centerTemplate;
                // Rotate point around the start
                dP_transformed = boreholeRotation * (dP_transformed - startTemplateGlobal) + startTemplateGlobal;
                // Move point to the borehole start
                dP_transformed = dP_transformed + translate;
                P_transformed = dP_transformed.cast<int>();
                // Check if the point is in range
                if (P_transformed.x() < 0 || P_transformed.x() >= inputData->length || P_transformed.y() < 0 || P_transformed.y() >= inputData->length || P_transformed.z() < 0 || P_transformed.z() >= inputData->layers) {
                    continue;
                }

                // Check if the point is in the borehole or if the value is greater than the threshold, if so remove all points in the increasing z direction.
                indexInput = P_transformed.x() + (P_transformed.y() * inputData->length) + (P_transformed.z() * inputData->length * inputData->length);
                if (inputData->data[indexInput] >= threshold || (boreholeCenter - P.cast<double>()).norm() <= double(diameter) / 2.0) {
                    for (int z2=z; z2>=0; z2--) {
                        templateData->data[x + (y * templateData->length) + (z2 * templateData->length * templateData->length)] = 0;
                    }
                }
            }
        }
    }
    return ReturnCode::OK;
}


/**
 * Returns the image data.
 * @return The image data.
 */
ImageData3D* CTDataset::data() {
    return m_pImageData;
}

/**
 * Returns the rotated image data.
 * @return The rotated image data.
 */
ImageData3D* CTDataset::rotatedData() {
    return m_pRotatedImageData;
}

/**
 * Returns the depth buffer.
 * @return The depth buffer.
 */
ImageData2D* CTDataset::depthBuffer() {
    return m_pDepthBuffer;
}

/**
 * Returns the shaded depth buffer.
 * @return The shaded depth buffer.
 */
ImageData2D* CTDataset::shadedDepthBuffer() {
    return m_pShadedDepthBuffer;
}

/**
 * Returns the template data.
 * @return The template.
 */
ImageData3D* CTDataset::templateData() {
    return m_pTemplate;
}