/**
 * @file
 * @author Tim Heß
 * @date 01.02.2024
 */

#ifndef SWE_IMAGEDATA2D_H
#define SWE_IMAGEDATA2D_H

/**
 * @brief The ImageData2D class is used to store square 2D image data.
 */
class ImageData2D {
public:
    short *data; //!< The actual image data.
    int length; //!< The length of one side of the square image.

    /**
     * @brief Default constructor of the ImageData2D class.
     * The data pointer is set to nullptr and the length is set to 0.
     */
    ImageData2D() : data(nullptr), length(0) {}

    /**
     * @brief Constructor of the ImageData2D class.
     * The data pointer is set to a new array of length*length and the corresponding length is set.
     * @param length The length of one side of the square image.
     */
    ImageData2D(int length) : data(new short[length*length] ), length(length) {}

    /**
     * @brief Destructor of the ImageData2D class.
     * The data pointer is deleted.
     */
    ~ImageData2D() { delete[] data; }

    /**
     * @brief Check if the image data is loaded.
     * @return True if the image data is loaded, false otherwise.
     */
    bool Loaded() {
        return data != nullptr;
    }

    /**
     * @brief Set the length of the image data.
     * The data is deleted and set to a new array of length*length and the corresponding length is set.
     * @param length The length of one side of the square image.
     */
    void Set(int length) {
        delete[] this->data;
        this->data = new short [length * length];
        this->length = length;
    }

    /**
     * @brief Scale the image data.
     * The data is scaled by the given factor.
     * @param factor The factor to scale the image data by. Only integer factors are supported.
     */
    void Scale(int factor) {
        short *newData = new short [length * factor * length * factor];
        for (int x = 0; x < length; x++) {
            for (int y = 0; y < length; y++) {
                for (int fx = 0; fx < factor; fx++) {
                    for (int fy = 0; fy < factor; fy++) {
                        newData[(x * factor + fx) + (y * factor + fy) * length * factor] = data[x + y * length];
                    }
                }
            }
        }
        delete[] this->data;
        this->data = newData;
        this->length *= factor;
    }

    /**
     * @brief Increase the size of the image data.
     * The data is placed in the center of a new array of newLength*newLength and the corresponding length is set.
     * @param newLength The new length of one side of the square image.
     * @param defaultValue The value to fill the new array with.
     */
    void IncreaseSize(int newLength, int defaultValue = -1024) {
        auto *newData = new short [newLength * newLength];
        std::fill(newData, newData + newLength * newLength, defaultValue);

        // Place old data in the center
        for (int x = 0; x < this->length; x++) {
            for (int y = 0; y < this->length; y++) {
                newData[(x + (newLength - this->length) / 2) + (y + (newLength - this->length) / 2) * newLength] = data[x + y * length];
            }
        }
        delete[] this->data;
        this->data = newData;
        this->length = newLength;
    }
};


#endif //SWE_IMAGEDATA2D_H
