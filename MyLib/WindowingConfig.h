/**
 * @file
 * @author Tim Heß
 * @date 02.02.2024
 */

#ifndef SWE_WINDOWINGCONFIG_H
#define SWE_WINDOWINGCONFIG_H

/** @brief The WindowingConfig struct conveniently stores the parameters required to perform windowing.
 */
struct WindowingConfig {
    int center; //!< The center of the windowing.
    int width; //!< The width of the windowing.
    int threshold; //!< The threshold for the windowing.
    bool showThreshold; //!< Flag to indicate if the threshold should be shown.
};

#endif //SWE_WINDOWINGCONFIG_H
