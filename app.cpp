/**
 * @file
 * @author Tim Heß
 * @date 18.10.2023
 */

// You may need to build the project (run Qt uic code generator) to get "ui_App.h" resolved

#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <QElapsedTimer>
#include <QMouseEvent>
#include <QVector3D>
#include <QString>
#include <qsignalmapper.h>
#include <qpainter.h>
#include "app.h"
#include "ui_App.h"

const int IMAGE_SIZE = 512;
QImage image_ct(IMAGE_SIZE, IMAGE_SIZE, QImage::Format_RGB32);
QImage image_3d(IMAGE_SIZE, IMAGE_SIZE, QImage::Format_RGB32);

App::App(QWidget *parent) :
        QWidget(parent), ui(new Ui::App) {
    ui->setupUi(this);
    connect(ui->pushButton_loadImage, SIGNAL(clicked()), this, SLOT(LoadImage()));
    connect(ui->pushButton_render3D, SIGNAL(clicked()), this, SLOT(Render3D()));
    connect(ui->horizontalSlider_center, SIGNAL(valueChanged(int)), this, SLOT(updateWindowingCenter(int)));
    connect(ui->horizontalSlider_width, SIGNAL(valueChanged(int)), this, SLOT(updateWindowingWidth(int)));
    connect(ui->horizontalSlider_layer, SIGNAL(valueChanged(int)), this, SLOT(updateLayer(int)));
    connect(ui->horizontalSlider_threshold, SIGNAL(valueChanged(int)), this, SLOT(updateThreshold(int)));
    connect(ui->spinBox_center, SIGNAL(valueChanged(int)), this, SLOT(updateWindowingCenter(int)));
    connect(ui->spinBox_width, SIGNAL(valueChanged(int)), this, SLOT(updateWindowingWidth(int)));
    connect(ui->spinBox_layer, SIGNAL(valueChanged(int)), this, SLOT(updateLayer(int)));
    connect(ui->spinBox_threshold, SIGNAL(valueChanged(int)), this, SLOT(updateThreshold(int)));
    connect(ui->checkBox_threshold, SIGNAL(stateChanged(int)), this, SLOT(enableThreshold(int)));

    // setup and connect page buttons
    ui->pushButton_page2->setEnabled(false);
    ui->pushButton_page3->setEnabled(false);
    ui->pushButton_page4->setEnabled(false);
    connect(ui->pushButton_page1, &QPushButton::clicked, [this] {updateAppView(0);});
    connect(ui->pushButton_page2, &QPushButton::clicked, [this] {updateAppView(1);});
    connect(ui->pushButton_page3, &QPushButton::clicked, [this] {updateAppView(2);});
    connect(ui->pushButton_page4, &QPushButton::clicked, [this] {updateAppView(3);});

    // setup and connect rotation buttons
    ui->rotationButtons->setVisible(true);
    connect(ui->pushButton_rotateXNegative, &QPushButton::clicked, [this] {rotateImage(0);});
    connect(ui->pushButton_rotateXPositive, &QPushButton::clicked, [this] {rotateImage(1);});
    connect(ui->pushButton_rotateYPositive, &QPushButton::clicked, [this] {rotateImage(2);});
    connect(ui->pushButton_rotateYNegative, &QPushButton::clicked, [this] {rotateImage(3);});
    connect(ui->pushButton_rotateZPositive, &QPushButton::clicked, [this] {rotateImage(4);});
    connect(ui->pushButton_rotateZNegative, &QPushButton::clicked, [this] {rotateImage(5);});
    connect(ui->pushButton_reset, &QPushButton::clicked, [this] {rotateImage(-1);});


    // setup and connect position spin boxes
    connect(ui->startX, SIGNAL(valueChanged(int)), this, SLOT(updatePositions(int)));
    connect(ui->startY, SIGNAL(valueChanged(int)), this, SLOT(updatePositions(int)));
    connect(ui->startZ, SIGNAL(valueChanged(int)), this, SLOT(updatePositions(int)));
    connect(ui->endX, SIGNAL(valueChanged(int)), this, SLOT(updatePositions(int)));
    connect(ui->endY, SIGNAL(valueChanged(int)), this, SLOT(updatePositions(int)));
    connect(ui->endZ, SIGNAL(valueChanged(int)), this, SLOT(updatePositions(int)));
    connect(ui->end_Length, SIGNAL(valueChanged(int)), this, SLOT(updateLength(int)));

    // setup and connect radio buttons
    connect(ui->radioButton_left_1, &QPushButton::clicked, [this] {updateVerifyView(0);});
    connect(ui->radioButton_left_2, &QPushButton::clicked, [this] {updateVerifyView(1);});
    connect(ui->radioButton_left_3, &QPushButton::clicked, [this] {updateVerifyView(2);});
    connect(ui->radioButton_left_4, &QPushButton::clicked, [this] {updateVerifyView(3);});
    connect(ui->radioButton_left_5, &QPushButton::clicked, [this] {updateVerifyView(4);});
    connect(ui->radioButton_right_1, &QPushButton::clicked, [this] {updateVerifyView(5);});
    connect(ui->radioButton_right_2, &QPushButton::clicked, [this] {updateVerifyView(6);});
    connect(ui->radioButton_right_3, &QPushButton::clicked, [this] {updateVerifyView(7);});
    connect(ui->radioButton_right_4, &QPushButton::clicked, [this] {updateVerifyView(8);});
    connect(ui->radioButton_right_5, &QPushButton::clicked, [this] {updateVerifyView(9);});

    connect(ui->selectStartPosition_button, SIGNAL(clicked()), this, SLOT(selectStartPosition()));
    connect(ui->selectEndPosition_button, SIGNAL(clicked()), this, SLOT(selectEndPosition()));

    // setup and connect borehole buttons
    connect(ui->pushButton_savePosition, SIGNAL(clicked()), this, SLOT(savePositions()));
    connect(ui->pushButton_saveTemplate, SIGNAL(clicked()), this, SLOT(saveTemplate()));
    connect(ui->spinBox_diameter, SIGNAL(valueChanged(int)), this, SLOT(updateDiameter(int)));

    iWindowingWidth = 800;
    iWindowingCenter = 0;
    iThreshold = 300;
    iSelectedImageIndex = 0;
    bThresholdEnabled = false;
    iCurrentPage = 1;

    iStartPosition = Eigen::Vector3i(-1,-1,-1);
    iEndPosition = Eigen::Vector3i(-1,-1,-1);

    rotation3D = Eigen::Matrix3d::Identity();
    rotationTemplate = Eigen::Matrix3d::Identity();


    image_ct.fill(qRgb(0,0,0));
    image_3d.fill(qRgb(0,0,0));
    updateAppView(0);
}

App::~App() {
    delete ui;
}

void App::updateAppView(int page) {
    switch (page) {
        case 0:
            setPage1();
            break;
        case 1:
            if (page1Complete()) {
                setPage2();
            }
            break;
        case 2:
            if (page2Complete()) {
                setPage3();
            }
            break;
        case 3:
            if (page3Complete()) {
                setPage4();
            }
            break;
        default:
            break;
    }
}

bool App::page1Complete() {
    bool res = (dataset.depthBuffer()->Loaded() && dataset.data()->Loaded());
    if (res && !ui->pushButton_page2->isEnabled()) {
        ui->pushButton_page2->setEnabled(true);
        ui->pushButton_page1->setIcon(QIcon("./Assets/check-solid-green.svg"));
        ui->pushButton_page2->setIcon(QIcon("./Assets/bore-hole-solid-green.svg"));
    } else if (!res && ui->pushButton_page2->isEnabled()) {
        ui->pushButton_page2->setEnabled(false);
        ui->pushButton_page1->setIcon(QIcon("./Assets/image-solid-green.svg"));
        ui->pushButton_page2->setIcon(QIcon("./Assets/bore-hole-solid.svg"));
    }
    return res;
}

bool App::page2Complete() {
    bool res = page1Complete() && bStartSelected && bEndSelected;
    if (res && !ui->pushButton_page3->isEnabled() ) {
        ui->pushButton_page3->setEnabled(true);
        ui->pushButton_page2->setIcon(QIcon("./Assets/check-solid-green.svg"));
        ui->pushButton_page3->setIcon(QIcon("./Assets/list-check-solid-green.svg"));
    } else if (!res && ui->pushButton_page3->isEnabled()) {
        ui->pushButton_page3->setEnabled(false);
        ui->pushButton_page2->setIcon(QIcon("./Assets/bore-hole-solid-green.svg"));
        ui->pushButton_page3->setIcon(QIcon("./Assets/list-check-solid.svg"));
        ui->pushButton_page4->setEnabled(false);
        ui->pushButton_page4->setIcon(QIcon("./Assets/file-medical-solid.svg"));
    }
    return res;
}

bool App::page3Complete() {
    bool res = (page2Complete());
    if (res && !ui->pushButton_page4->isEnabled()) {
        ui->pushButton_page4->setEnabled(true);
        ui->pushButton_page3->setIcon(QIcon("./Assets/check-solid-green.svg"));
        ui->pushButton_page4->setIcon(QIcon("./Assets/file-medical-solid-green.svg"));
    } else if (!res && ui->pushButton_page4->isEnabled()) {
        ui->pushButton_page4->setEnabled(false);
        ui->pushButton_page3->setIcon(QIcon("./Assets/list-check-solid-green.svg"));
        ui->pushButton_page4->setIcon(QIcon("./Assets/file-medical-solid.svg"));
    }
    return res;
}

bool App::page4Complete() {
    bool res = (page3Complete() && savedTemplate && savedPositions);
    if (res) {
        ui->pushButton_page4->setIcon(QIcon("./Assets/check-solid-green.svg"));
    } else {
        ui->pushButton_page4->setIcon(QIcon("./Assets/file-medical-solid-green.svg"));
    }
    return res;
}

void App::setPage1() {
    iCurrentPage = 1;
    ui->pushButton_page1->setStyleSheet("background-color: rgb(86, 87, 94);");
    ui->pushButton_page2->setStyleSheet("");
    ui->pushButton_page3->setStyleSheet("");
    ui->pushButton_page4->setStyleSheet("");
    ui->image_left->setPixmap(QPixmap::fromImage(image_ct));
    ui->image_right->setPixmap(QPixmap::fromImage(image_3d));
    ui->rotationButtons->setVisible(true);
    ui->stackedWidget->setCurrentIndex(0);
    ui->horizontalSlider_layer->setValue(iSelectedImageIndex);
    ui->spinBox_layer->setValue(iSelectedImageIndex);
}

void App::setPage2() {
    iCurrentPage = 2;
    ui->pushButton_page2->setStyleSheet("background-color: rgb(86, 87, 94);");
    ui->pushButton_page1->setStyleSheet("");
    ui->pushButton_page3->setStyleSheet("");
    ui->pushButton_page4->setStyleSheet("");
    ui->rotationButtons->setVisible(true);
    ui->image_left->setPixmap(QPixmap::fromImage(image_ct));
    ui->image_right->setPixmap(QPixmap::fromImage(image_3d));
    ui->stackedWidget->setCurrentIndex(1);
    ui->endZ->setMaximum(dataset.data()->layers-1);
    ui->endZ->setValue(iSelectedImageIndex);
    ui->startX->setValue(100);
    ui->startY->setValue(265);
    ui->startZ->setValue(40);
    ui->endX->setValue(150);
    ui->endY->setValue(240);
    ui->endZ->setValue(90);
    updateBoreView();
}

void App::setPage3() {
    iCurrentPage = 3;
    ui->pushButton_page3->setStyleSheet("background-color: rgb(86, 87, 94);");
    ui->pushButton_page1->setStyleSheet("");
    ui->pushButton_page2->setStyleSheet("");
    ui->pushButton_page4->setStyleSheet("");
    ui->stackedWidget->setCurrentIndex(2);

    updateVerifyView(1);
    updateVerifyView(5);
    page3Complete();
}

void App::setPage4() {
    iCurrentPage = 4;
    ui->pushButton_page4->setStyleSheet("background-color: rgb(86, 87, 94);");
    ui->pushButton_page1->setStyleSheet("");
    ui->pushButton_page2->setStyleSheet("");
    ui->pushButton_page3->setStyleSheet("");
    ui->rotationButtons->setVisible(true);
    ui->stackedWidget->setCurrentIndex(3);
    updateVerifyView(0);
    this->displayTemplate();
}

void App::mousePressEvent(QMouseEvent *event) {
    QPoint globalPos = event->pos();

    QPoint localPosLeft = ui->image_left->mapFromParent(globalPos);
    QPoint localPosRight = ui->image_right->mapFromParent(globalPos);

    if (iCurrentPage == 1) {
        if (ui->image_right->rect().contains(localPosRight) && dataset.depthBuffer()->Loaded() && dataset.data()->Loaded()) {
            Eigen::Vector3i selectedPoint =
                    Eigen::Vector3i(localPosRight.x(),
                                    localPosRight.y(),
                                    dataset.depthBuffer()->data[localPosRight.x() + localPosRight.y() * IMAGE_SIZE]);
            Eigen::Vector3i pos = CTDataset::reverseTransformPoint(selectedPoint,&rotation3D, dataset.data());
            if (!(pos.z() < 0 || pos.z() >= dataset.data()->layers)) {
                this->updateLayer(pos.z());
            }

        }
    }

    if (iCurrentPage == 2) {
        if (!bEndSelected && ui->image_left->rect().contains(localPosLeft)) {
            iEndPositionImage = Eigen::Vector3i(localPosLeft.x(), localPosLeft.y(), iSelectedImageIndex);
            iEndPosition = iEndPositionImage;
            iClickedEndPosition = iEndPosition;
            ui->endX->blockSignals(true);ui->endX->setValue(iEndPositionImage.x());ui->endX->blockSignals(false);
            ui->endY->blockSignals(true);ui->endY->setValue(iEndPositionImage.y());ui->endY->blockSignals(false);
            ui->endZ->blockSignals(true);ui->endZ->setValue(iEndPositionImage.z());ui->endZ->blockSignals(false);
            ui->end_Length->blockSignals(true);
            ui->end_Length->setValue((iEndPosition-iStartPosition).norm());
            ui->end_Length->blockSignals(false);
            updateBoreView();
        }
        if (!bStartSelected && ui->image_right->rect().contains(localPosRight)) {
            iStartPositionImage = Eigen::Vector3i(localPosRight.x(), localPosRight.y(),
                                                  dataset.depthBuffer()->data[localPosRight.x() +
                                                          localPosRight.y() * IMAGE_SIZE]);
            iStartPosition = CTDataset::reverseTransformPoint(iStartPositionImage, &rotation3D, dataset.data());
            ui->startX->blockSignals(true);ui->startX->setValue(iStartPosition.x());ui->startX->blockSignals(false);
            ui->startY->blockSignals(true);ui->startY->setValue(iStartPosition.y());ui->startY->blockSignals(false);
            ui->startZ->blockSignals(true);ui->startZ->setValue(iStartPosition.z());ui->startZ->blockSignals(false);
            ui->end_Length->blockSignals(true);
            ui->end_Length->setValue((iEndPosition-iStartPosition).norm());
            ui->end_Length->blockSignals(false);
            ui->endZ->blockSignals(true);ui->endZ->setValue(iStartPosition.z()); ui->endZ->blockSignals(false);
            if (!(iStartPosition.z() < 0 || iStartPosition.z() >= dataset.data()->layers)) {
                iSelectedImageIndex = iStartPosition.z();
                this->updateLayer(iSelectedImageIndex);
            }
            updateBoreView();
        }
    }
}

void App::rotateImage(int rotationIndex) {
    if (!dataset.shadedDepthBuffer()->Loaded()) return;

    if (iCurrentPage==1 || iCurrentPage==2) {
        CTDataset::applyRotation(rotationIndex, &rotation3D);
        this->Render3D();
    }

    if (iCurrentPage==3) {
        CTDataset::applyRotation(rotationIndex, &rotation3D);
        this->Render3D(false);
        this->updateVerifyView(5);
    }

    if (iCurrentPage==4) {
        CTDataset::applyRotation(rotationIndex, &rotationTemplate);
        this->displayTemplate();
    }
}

void App::LoadImage() {
    delete[] dataset.data()->data;
    delete[] dataset.depthBuffer()->data;
    dataset.data()->data = nullptr;
    dataset.depthBuffer()->data = nullptr;
    this->page1Complete();
    setControls(false);
    image_ct.fill(qRgb(0,0,0));
    ui->image_left->setPixmap(QPixmap::fromImage(image_ct));

    image_3d.fill(qRgb(0,0,0));
    ui->image_right->setPixmap(QPixmap::fromImage(image_3d));

    QString imagePath = QFileDialog::getOpenFileName(this, "Open Image", "../CTScans",
                                                     "Raw Image Files (*.raw)", nullptr, QFileDialog::DontUseNativeDialog);
    if (imagePath.isEmpty()) {
        return;
    }

    if (CTDataset::load(imagePath, IMAGE_SIZE, dataset.data()) != ReturnCode::OK) {
        QMessageBox::critical(this, "ACHTUNG", "Datei konnte nicht geöffnet werden");
        return;
    }

    ui->horizontalSlider_layer->setMaximum(dataset.data()->layers-1);
    ui->spinBox_layer->setMaximum(dataset.data()->layers-1);
    ui->label_layer->setText("Layer ("+QString::number(dataset.data()->layers)+")");
    this->setControls(true);
    this->updateLayer(dataset.data()->layers-1);

}

void App::updateBoreView() {
    if (iStartPosition.x() != -1 && iStartPosition.y() != -1 && iStartPosition.z() != -1) {
        QImage crosshairImage3D = image_3d.copy(0, 0, IMAGE_SIZE, IMAGE_SIZE);
        QPainter painter3D(&crosshairImage3D);
        if (bStartSelected) {
            painter3D.setPen(QPen(Qt::green, 1, Qt::SolidLine));
        } else {
            painter3D.setPen(QPen(Qt::red, 1, Qt::SolidLine));
        }
        painter3D.drawLine(0, iStartPositionImage.y(), IMAGE_SIZE, iStartPositionImage.y());
        painter3D.drawLine(iStartPositionImage.x(), 0, iStartPositionImage.x(), IMAGE_SIZE);
        ui->image_right->setPixmap(QPixmap::fromImage(crosshairImage3D));
    }

    if (iEndPosition.x() != -1 && iEndPosition.y() != -1 && iEndPosition.z() != -1){

        QImage crosshairImage = image_ct.copy(0, 0, IMAGE_SIZE, IMAGE_SIZE);
        QPainter painter(&crosshairImage);
        if (bEndSelected) {
            painter.setPen(QPen(Qt::green, 1, Qt::SolidLine));
        } else {
            painter.setPen(QPen(Qt::red, 1, Qt::SolidLine));
        }
        painter.drawLine(0, iEndPositionImage.y(), IMAGE_SIZE, iEndPositionImage.y());
        painter.drawLine(iEndPositionImage.x(), 0, iEndPositionImage.x(), IMAGE_SIZE);
        ui->image_left->setPixmap(QPixmap::fromImage(crosshairImage));
    }
}

void App::Render3D(bool updateImage) {
    if (!dataset.data()->Loaded()) return;

    CTDataset::rotateData(dataset.data(), &rotation3D, dataset.rotatedData());

    if (ReturnCode::OK != CTDataset::calculateDepthBuffer(
            dataset.rotatedData(),
            iThreshold,
            dataset.depthBuffer()))
    {
        QMessageBox::critical(this, "ACHTUNG", "Fehler beim Berechnen des Depth Buffers");
        return;
    }

    if (ReturnCode::OK != CTDataset::calculateShadedDepthBuffer(
            dataset.depthBuffer(),
            dataset.shadedDepthBuffer()))
    {
        QMessageBox::critical(this, "ACHTUNG", "Fehler beim Rendern des Depth Buffers");
        return;
    }
    if (ReturnCode::OK != CTDataset::renderShadedDepthBuffer(&image_3d, dataset.shadedDepthBuffer())) {
        QMessageBox::critical(this, "ACHTUNG", "Fehler beim Rendern des Shaded Depth Buffers");
        return;
    }
    if (updateImage) {
        ui->image_right->setPixmap(QPixmap::fromImage(image_3d));
    }
    page1Complete();
}

void App::updateCTImage(QLabel *image) {
    WindowingConfig windowingConfig = {
            iWindowingCenter,
            iWindowingWidth,
            iThreshold,
            bThresholdEnabled
    };
    if (ReturnCode::OK != CTDataset::renderSliceView(
            &image_ct,
            dataset.data(),
            iSelectedImageIndex,
            windowingConfig)) {
        QMessageBox::critical(this, "ACHTUNG", "Fehler beim Anzeigen des CT-Bildes");
    }
    image->setPixmap(QPixmap::fromImage(image_ct));
}

void App::updateWindowingWidth(int value) {
    this->ui->spinBox_width->blockSignals(true);
    this->ui->spinBox_width->setValue(value);
    this->ui->spinBox_width->blockSignals(false);
    this->ui->horizontalSlider_width->blockSignals(true);
    this->ui->horizontalSlider_width->setValue(value);
    this->ui->horizontalSlider_width->blockSignals(false);
    this->iWindowingWidth = value;
    updateCTImage(ui->image_left);
}

void App::updateWindowingCenter(int value) {
    this->ui->spinBox_center->blockSignals(true);
    this->ui->spinBox_center->setValue(value);
    this->ui->spinBox_center->blockSignals(false);
    this->ui->horizontalSlider_center->blockSignals(true);
    this->ui->horizontalSlider_center->setValue(value);
    this->ui->horizontalSlider_center->blockSignals(false);
    this->iWindowingCenter = value;
    updateCTImage(ui->image_left);
}

void App::updateLayer(int value) {
    this->ui->spinBox_layer->blockSignals(true);
    this->ui->spinBox_layer->setValue(value);
    this->ui->spinBox_layer->blockSignals(false);
    this->ui->horizontalSlider_layer->blockSignals(true);
    this->ui->horizontalSlider_layer->setValue(value);
    this->ui->horizontalSlider_layer->blockSignals(false);
    this->iSelectedImageIndex = value;
    updateCTImage(ui->image_left);
}

void App::updateThreshold(int value) {
    this->ui->spinBox_threshold->blockSignals(true);
    this->ui->spinBox_threshold->setValue(value);
    this->ui->spinBox_threshold->blockSignals(false);
    this->ui->horizontalSlider_threshold->blockSignals(true);
    this->ui->horizontalSlider_threshold->setValue(value);
    this->ui->horizontalSlider_threshold->blockSignals(false);
    this->iThreshold = value;
    updateCTImage(ui->image_left);
}

void App::enableThreshold(int value) {
    this->bThresholdEnabled = value == 2;
    updateCTImage(ui->image_left);
}

void App::setControls(bool state) {
    ui->horizontalSlider_layer->setEnabled(state);
    ui->horizontalSlider_center->setEnabled(state);
    ui->horizontalSlider_width->setEnabled(state);
    ui->horizontalSlider_threshold->setEnabled(state);

    ui->spinBox_center->setEnabled(state);
    ui->spinBox_width->setEnabled(state);
    ui->spinBox_layer->setEnabled(state);
    ui->spinBox_threshold->setEnabled(state);

    ui->checkBox_threshold->setEnabled(state);
    ui->pushButton_render3D->setEnabled(state);
}

void App::updatePositions(int value) {
    iStartPosition = Eigen::Vector3i(ui->startX->value(), ui->startY->value(), ui->startZ->value());
    iStartPositionImage = CTDataset::transformPoint(iStartPosition, &rotation3D, dataset.data());
    iEndPosition = Eigen::Vector3i(ui->endX->value(), ui->endY->value(), ui->endZ->value());
    iEndPositionImage = iEndPosition;
    iClickedEndPosition = iEndPosition;
    iSelectedImageIndex = iEndPosition.z();
    ui->end_Length->blockSignals(true);
    ui->end_Length->setValue((iEndPosition-iStartPosition).norm());
    ui->end_Length->blockSignals(false);
    updateCTImage(ui->image_left);
    updateBoreView();
}

void App::updateLength(int value) {
    Eigen::Vector3d dir = iClickedEndPosition.cast<double>()-iStartPosition.cast<double>();
    dir.normalize();
    Eigen::Vector3d end = iStartPosition.cast<double>() + dir*value;
    iEndPosition = end.cast<int>();
    iEndPositionImage = CTDataset::transformPoint(iEndPosition, &rotation3D, dataset.data());
    ui->endX->blockSignals(true);ui->endX->setValue(iEndPositionImage.x());ui->endX->blockSignals(false);
    ui->endY->blockSignals(true);ui->endY->setValue(iEndPositionImage.y());ui->endY->blockSignals(false);
    ui->endZ->blockSignals(true);ui->endZ->setValue(iEndPositionImage.z());ui->endZ->blockSignals(false);
    iSelectedImageIndex = iEndPosition.z();
    updateCTImage(ui->image_left);
    updateBoreView();
}

void App::selectStartPosition() {
    if (!bStartSelected && iStartPosition.x() != -1 && iStartPosition.y() != -1 && iStartPosition.z() != -1) {
        bStartSelected = true;
        ui->selectStartPosition_button->setText("Unselect Start");
        ui->startX->setEnabled(false);
        ui->startY->setEnabled(false);
        ui->startZ->setEnabled(false);
    } else {
        bStartSelected = false;
        ui->selectStartPosition_button->setText("Select Start");
        ui->startX->setEnabled(true);
        ui->startY->setEnabled(true);
        ui->startZ->setEnabled(true);
    }
    updateBoreView();
    page2Complete();
}

void App::selectEndPosition() {
    if (!bEndSelected && iEndPosition.x() != -1 && iEndPosition.y() != -1 && iEndPosition.z() != -1) {
        bEndSelected = true;
        ui->selectEndPosition_button->setText("Unselect Start");
        ui->endX->setEnabled(false);
        ui->endY->setEnabled(false);
        ui->endZ->setEnabled(false);
        ui->end_Length->setEnabled(false);
    } else {
        bEndSelected = false;
        ui->selectEndPosition_button->setText("Select Start");
        ui->endX->setEnabled(true);
        ui->endY->setEnabled(true);
        ui->endZ->setEnabled(true);
        ui->end_Length->setEnabled(true);
    }
    updateBoreView();
    page2Complete();
}

void App::updateVerifyView(int view) {

    // Direction of the borehole
    Eigen::Vector3d dir = (iEndPosition.cast<double>()-iStartPosition.cast<double>()).normalized();
    int length = (iEndPosition-iStartPosition).norm();
    //Length of the borehole
    int sliceLength = length * 3;
    if (sliceLength > IMAGE_SIZE) {
        sliceLength = IMAGE_SIZE;
    }
    // Center of the borehole
    Eigen::Vector3i center = ((iEndPosition.cast<double>()+iStartPosition.cast<double>())/2).cast<int>();

    // Image for the slice
    QImage sliceImage = QImage(sliceLength, sliceLength, QImage::Format_RGB32);
    QImage renderImage = QImage(IMAGE_SIZE, IMAGE_SIZE, QImage::Format_RGB32);
    auto* slice = new ImageData2D(IMAGE_SIZE);

    // Direction of the slice
    Eigen::Vector3d dXDir, dYDir, dNormal, dTemp1, dTemp2;
    Eigen::Vector3i iStartImage, iEndImage, iCenterImage, iDirImage;
    int lengthImage;
    QPainter painter;

    WindowingConfig windowingConfig = {
            iWindowingCenter,
            iWindowingWidth,
            iThreshold,
            bThresholdEnabled
    };

    if (view / 5 == 1) {
        ui->rotationButtons->setVisible(false);
    }

    switch (view % 5) {
        case 0:
            ui->rotationButtons->setVisible(true);
            // Caculate positions of the borehole in the image
            iStartImage = CTDataset::transformPoint(iStartPosition, &rotation3D, dataset.data());
            iEndImage = CTDataset::transformPoint(iEndPosition, &rotation3D, dataset.data());
            iCenterImage = (iStartImage + iEndImage) / 2;
            lengthImage = int((iEndImage - iStartImage).norm());
            iDirImage = ((iEndImage.cast<double>() - iStartImage.cast<double>()).normalized() * lengthImage * 0.25).cast<int>();
            CTDataset::renderShadedDepthBuffer(&renderImage, dataset.shadedDepthBuffer());
            break;
        case 1:
            dXDir = dir;
            dYDir = dir.cross(Eigen::Vector3d::UnitX());

            iStartImage = CTDataset::convertToPlaneCoordinates(iStartPosition.cast<double>(), dXDir, dYDir, center.cast<double>(), slice).cast<int>();
            iEndImage = CTDataset::convertToPlaneCoordinates(iEndPosition.cast<double>(), dXDir, dYDir, center.cast<double>(), slice).cast<int>();
            lengthImage = int((iEndImage - iStartImage).norm());
            iDirImage = ((iEndImage.cast<double>() - iStartImage.cast<double>()).normalized() * lengthImage * 0.25).cast<int>();
            iCenterImage = (iStartImage + iEndImage) / 2;
            CTDataset::getSlice(dataset.data(), center, dXDir, dYDir, slice);
            CTDataset::renderSliceView(&renderImage, slice, windowingConfig);
            break;
        case 2:
            dXDir = dir;
            dYDir = dir.cross(Eigen::Vector3d::UnitY());

            iStartImage = CTDataset::convertToPlaneCoordinates(iStartPosition.cast<double>(), dXDir, dYDir, center.cast<double>(), slice).cast<int>();
            iEndImage = CTDataset::convertToPlaneCoordinates(iEndPosition.cast<double>(), dXDir, dYDir, center.cast<double>(), slice).cast<int>();
            lengthImage = int((iEndImage - iStartImage).norm());
            iDirImage = ((iEndImage.cast<double>() - iStartImage.cast<double>()).normalized() * lengthImage * 0.25).cast<int>();
            iCenterImage = (iStartImage + iEndImage) / 2;
            CTDataset::getSlice(dataset.data(), center, dXDir, dYDir, slice);
            CTDataset::renderSliceView(&renderImage, slice, windowingConfig);
            break;
        case 3:
            dXDir = dir;
            dYDir = dir.cross(Eigen::Vector3d::UnitZ());

            iStartImage = CTDataset::convertToPlaneCoordinates(iStartPosition.cast<double>(), dXDir, dYDir, center.cast<double>(), slice).cast<int>();
            iEndImage = CTDataset::convertToPlaneCoordinates(iEndPosition.cast<double>(), dXDir, dYDir, center.cast<double>(), slice).cast<int>();
            lengthImage = int((iEndImage - iStartImage).norm());
            iDirImage = ((iEndImage.cast<double>() - iStartImage.cast<double>()).normalized() * lengthImage * 0.25).cast<int>();
            iCenterImage = (iStartImage + iEndImage) / 2;
            CTDataset::getSlice(dataset.data(), center, dXDir, dYDir, slice);
            CTDataset::renderSliceView(&renderImage, slice, windowingConfig);
            break;
        case 4:
            dXDir = Eigen::Vector3d::UnitX().cross(dir);
            dYDir = Eigen::Vector3d::UnitY().cross(dir);
            CTDataset::getSlice(dataset.data(), center, dXDir, dYDir, slice);

            iStartImage = CTDataset::convertToPlaneCoordinates(iStartPosition.cast<double>(), dXDir, dYDir, center.cast<double>(), slice).cast<int>();
            iEndImage = CTDataset::convertToPlaneCoordinates(iEndPosition.cast<double>(), dXDir, dYDir, center.cast<double>(), slice).cast<int>();
            lengthImage = int((iEndImage - iStartImage).norm());
            iDirImage = ((iEndImage.cast<double>() - iStartImage.cast<double>()).normalized() * lengthImage * 0.25).cast<int>();
            iCenterImage = (iStartImage + iEndImage) / 2;
            CTDataset::renderSliceView(&renderImage, slice, windowingConfig);
            break;
    }

    painter.begin(&renderImage);
    painter.setPen(QPen(Qt::green, 1, Qt::SolidLine));
    painter.drawLine(iStartImage.x(), iStartImage.y(), iEndImage.x(), iEndImage.y());
    painter.setPen(QPen(Qt::red, 1, Qt::SolidLine));
    painter.drawLine(iCenterImage.x() - iDirImage.y(), iCenterImage.y() + iDirImage.x(), iCenterImage.x() + iDirImage.y(), iCenterImage.y() - iDirImage.x());
    sliceImage = renderImage.copy(iCenterImage.x() - sliceLength / 2, iCenterImage.y() - sliceLength / 2, sliceLength, sliceLength);
    switch(view/5) {
        case 0:
            ui->image_left->setPixmap(QPixmap::fromImage(sliceImage.scaled(IMAGE_SIZE, IMAGE_SIZE, Qt::KeepAspectRatio)));
            break;
        case 1:
            ui->image_right->setPixmap(QPixmap::fromImage(sliceImage.scaled(IMAGE_SIZE, IMAGE_SIZE, Qt::KeepAspectRatio)));
            break;
    }
}

void App::updateDiameter(int value) {
    ui->spinBox_diameter->blockSignals(true);
    ui->spinBox_diameter->setValue(value);
    ui->spinBox_diameter->blockSignals(false);
    displayTemplate();
}

void App::displayTemplate() {
    savedPositions = false;
    savedTemplate = false;
    page4Complete();
    if (ReturnCode::OK != CTDataset::createTemplate(
            dataset.data(),
            iThreshold,
            ui->spinBox_diameter->value(),
            iStartPosition.cast<double>(),
            iEndPosition.cast<double>(),
            dataset.templateData())) {
        QMessageBox::critical(this, "ACHTUNG", "Fehler beim Erstellen des Templates");
    }
    // Scale the template and increase buffer size for rotation
    dataset.templateData()->Scale(5);
    dataset.templateData()->IncreaseSize(IMAGE_SIZE*0.75, IMAGE_SIZE*0.75, 0);

    auto* tmpl = new ImageData3D();
    CTDataset::rotateData(dataset.templateData(), &rotationTemplate, tmpl);
    QImage templateImage = QImage(IMAGE_SIZE, IMAGE_SIZE, QImage::Format_RGB32);
    auto* depthBuffer = new ImageData2D();
    auto* shadedDepthBuffer = new ImageData2D();

    if (ReturnCode::OK != CTDataset::calculateDepthBuffer(tmpl, 1, depthBuffer)) {
        QMessageBox::critical(this, "ACHTUNG", "Fehler beim Berechnen des Depth Buffers des Templates");
        return;
    }
    if (ReturnCode::OK != CTDataset::calculateShadedDepthBuffer(depthBuffer, shadedDepthBuffer)) {
        QMessageBox::critical(this, "ACHTUNG", "Fehler beim Rendern des Shaded Depth Buffers des Templates");
        return;
    }
    shadedDepthBuffer->IncreaseSize(IMAGE_SIZE, 255);
    CTDataset::renderShadedDepthBuffer(&templateImage, shadedDepthBuffer);
    ui->image_right->setPixmap(QPixmap::fromImage(templateImage));
    delete tmpl;
    delete depthBuffer;
    delete shadedDepthBuffer;
}

void App::savePositions() {
    QString filename = QFileDialog::getSaveFileName(this, "Save Positions", "../CTScans/positions.txt", "Tex Files (*.txt)", nullptr, QFileDialog::DontUseNativeDialog);
    if (filename.isEmpty()) {
        return;
    }
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox::critical(this, "ACHTUNG", "Datei konnte nicht geöffnet werden");
        return;
    }
    QTextStream out(&file);
    out << "Start: " << iStartPosition.x() << " " << iStartPosition.y() << " " << iStartPosition.z() << "\n";
    out << "End: " << iEndPosition.x() << " " << iEndPosition.y() << " " << iEndPosition.z() << "\n";
    out << "Diameter: " <<  ui->spinBox_diameter->value() << "\n";
    file.close();
    savedPositions = true;
    page4Complete();
}

void App::saveTemplate() {
    // Save template as raw file
    QString filename = QFileDialog::getSaveFileName(this, "Save Template", "../CTScans/template.raw", "Raw Files (*.raw)", nullptr, QFileDialog::DontUseNativeDialog);
    if (filename.isEmpty()) {
        return;
    }
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox::critical(this, "ACHTUNG", "Datei konnte nicht geöffnet werden");
        return;
    }
    QDataStream out(&file);
    out.writeRawData((char*)dataset.templateData(), 40*40*40*sizeof(short));
    file.close();
    savedTemplate = true;
    page4Complete();

}