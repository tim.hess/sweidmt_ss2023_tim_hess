//
// Created by Tim on 29.11.2023.
//
#include <QString>
#include <QtTest>
#include "ctdataset.h"
#include <algorithm>

class MyLibTestClass : public QObject
{
    Q_OBJECT

public:
    MyLibTestClass() = default;

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void windowingTest();
};

void MyLibTestClass::initTestCase()
{
    // if necessary do some initialization stuff here before the tests start
}

void MyLibTestClass::cleanupTestCase()
{
    // if necessary do some clean up stuff here after all tests have finished
}

void MyLibTestClass::windowingTest()
{
    // VALID case 1: testing clean zero for bottom HU boundary
    ReturnCode retCode;
    CTDataset dataset;
    int greyValue = 0;


    retCode = CTDataset::windowing(-34, {-34 + 50, 100}, greyValue);
    QVERIFY2(retCode == ReturnCode::OK, "returns an error although input is valid");
    QVERIFY2(greyValue == 0, qPrintable(QString("windowing function lower bound, was %1 instead of 0").arg(greyValue)));

    // VALID case 2: testing clean 255 for top HU boundary
    retCode = CTDataset::windowing(36, {36 - 50, 100}, greyValue);
    QVERIFY2(retCode == ReturnCode::OK, "returns an error although input is valid");
    QVERIFY2(greyValue == 255, qPrintable(QString("windowing function upper bound, was %1 instead of 255").arg(greyValue)));

    // VALID case 3: testing center of windowed domain
    greyValue = -1;
    retCode = CTDataset::windowing(50, {50, 100}, greyValue);
    QVERIFY2(retCode == ReturnCode::OK, "returns an error although input is valid");
    QVERIFY2(greyValue == 128, qPrintable(QString("windowing function medium value, was %1 instead of 128").arg(greyValue)));

    // Valid case 4: HU value at global lower boundary
    greyValue = -1;
    retCode = CTDataset::windowing(-1024, {0, 2000}, greyValue);
    QVERIFY2(retCode == ReturnCode::OK, "returns an error although input is valid");
    QVERIFY2(greyValue == 0, qPrintable(QString("windowing function at lower HU boundary, was %1 instead of 0").arg(greyValue)));

    // Valid case 5: HU value at global upper boundary
    greyValue = -1;
    retCode = CTDataset::windowing(3071, {0, 2000}, greyValue);
    QVERIFY2(retCode == ReturnCode::OK, "returns an error although input is valid");
    QVERIFY2(greyValue == 255, qPrintable(QString("windowing function at upper HU boundary, was %1 instead of 255").arg(greyValue)));

    // INVALID case 1: HU input too low
    retCode = CTDataset::windowing(-4100, {-1000, 2000}, greyValue);
    QVERIFY2(retCode == ReturnCode::HU_OUT_OF_RANGE, "No error code returned although input HU value was <-1024");

    // INVALID case 2: HU input too high
    retCode = CTDataset::windowing(4200, {-1000, 2000}, greyValue);
    QVERIFY2(retCode == ReturnCode::HU_OUT_OF_RANGE, "No error code returned although input HU value was >3071");

    // INVALID case 3: Center value too low
    retCode = CTDataset::windowing(500, {-2100, 2000}, greyValue);
    QVERIFY2(retCode == ReturnCode::CENTER_OUT_OF_RANGE, "No error code returned although input center value was <-1024");

    // INVALID case 4: Center value too high
    retCode = CTDataset::windowing(500, {4000, 2000}, greyValue);
    QVERIFY2(retCode == ReturnCode::CENTER_OUT_OF_RANGE, "No error code returned although input center value was >3071");

    // INVALID case 5: Width value too low
    retCode = CTDataset::windowing(500, {1000, 0}, greyValue);
    QVERIFY2(retCode == ReturnCode::WIDTH_OUT_OF_RANGE, "No error code returned although input width value was <1");

    // INVALID case 6: Width value too high
    retCode = CTDataset::windowing(500, {1000, 4500}, greyValue);
    QVERIFY2(retCode == ReturnCode::WIDTH_OUT_OF_RANGE, "No error code returned although input width value was >4095");
}

QTEST_APPLESS_MAIN(MyLibTestClass)

#include "tst_mylibunittest.moc"
