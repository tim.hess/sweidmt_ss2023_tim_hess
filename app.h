/**
 * @file
 * @author Tim Heß
 * @date 18.10.2023
 */

#ifndef APP_APP_H
#define APP_APP_H

#include <QWidget>
#include <QLabel>
#include "MyLib/CTDataset.h"


QT_BEGIN_NAMESPACE
namespace Ui { class App; }
QT_END_NAMESPACE


/**
 * The App class is the main class of the application.
 */
class App : public QWidget {
Q_OBJECT

public:

    explicit App(QWidget *parent = nullptr);
    ~App() override;

private:

    Ui::App *ui;
    CTDataset dataset;

    int iWindowingWidth;
    int iWindowingCenter;
    int iSelectedImageIndex;
    int iThreshold;
    bool bThresholdEnabled;
    int iCurrentPage;


    Eigen::Vector3i iStartPosition, iStartPositionImage;
    Eigen::Vector3i iEndPosition, iEndPositionImage, iClickedEndPosition;

    Eigen::Matrix3d rotation3D, rotationTemplate;

    bool bStartSelected = false;
    bool bEndSelected = false;
    bool savedPositions = false;
    bool savedTemplate = false;

    void setControls(bool state);

    bool page1Complete();
    bool page2Complete();
    bool page3Complete();
    bool page4Complete();

    void setPage1();
    void setPage2();
    void setPage3();
    void setPage4();
    void updateBoreView();

    void updateCTImage(QLabel *image);

private slots:
    void LoadImage();
    void Render3D(bool updateImage = true);
    void updateWindowingCenter(int value);
    void updateWindowingWidth(int value);
    void updateLayer(int value);
    void updateThreshold(int value);
    void enableThreshold(int enable);
    void mousePressEvent(QMouseEvent *event) override;
    void updateAppView(int page);
    void rotateImage(int rotationIndex);
    void selectStartPosition();
    void selectEndPosition();
    void updatePositions(int value);
    void updateVerifyView(int view);
    void updateLength(int value);
    void savePositions();
    void saveTemplate();
    void updateDiameter(int value);
    void displayTemplate();
};

#endif //APP_APP_H
