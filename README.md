# SWEIDMT_SS2023_TIM_HESS - 370477

### Zweckbestimmung:
Entwicklung eines Operationsplanungssystems zur Dekompression einer atraumatischen Femurkopfnekrose.

### Online-Versionen:
Dokumentation: http://tim.hess.pages.rwth-aachen.de/sweidmt_ss2023_tim_hess
Pflichtenheft: https://tim.hess.pages.rwth-aachen.de/sweidmt_ss2023_tim_hess/Pflichtenheft.pdf

### Allgemeine Informationen
Projekt ist nur getestet mit den Compilern ``gcc`` und ``mingw``, da msvc nicht auf linux verfügbar ist.

### Beschreibung

QT Anwendung zur Planung einer Bohrung in einem 3D-Datensatz. 
Die Anwendung ermöglicht das Einladen und Visualisieren von 3D-Datensätzen, die Planung einer Bohrung, die Verifizierung der Bohrung und die Berechnung einer Bohrschablone.
![GUI](./Docs/gui_1.PNG)

Alle CT-Daten werden durch die Klasse CTDataset verwaltet, welche durch verschiedene Methoden der GUI verändert werden kann. Bilddaten werden Dank den Klassen ImageData2D und ImageData3D gespeichert.
![Klassendiagramm](./Docs/Klassendiagramm.png)

### Bedienungsanleitung:

Die Anwendung ist in vier Arbeitsschritte unterteilt. Jeder Arbeitsschritt ist in einem eigenen Tab in der GUI dargestellt. Die Arbeitsschritte sind:
1. Datensatz einladen und darstellen
2. Bohrung planen
3. Bohrung verifizieren
4. Bohrschablone berechnen

Die Arbeitsschritte können durch die Buttons im oberen Bereich der GUI gewechselt werden.
Abgeschlossene Arbeitsschritte werden durch einen grünen Haken markiert.
Verfügbare Arbeitsschritte durch ein blaues Icon.

#### Datensatz einladen und darstellen

Bei Start der Anwendung wird Ihnen die GUI für den ersten Arbeitsschritt angezeigt. Folgen Sie diesen Schritten, um einen Datensatz zu laden und zu visualisieren:

![GUI](./Docs/Gui/Folie1.PNG)

1. Klicken Sie auf den "Load Image" Button (1), um einen Datensatz zu laden. Die Visualisierung des Datensatz wird in Ansicht (A) dargestellt. 
2. Stellen Sie die Parameter für die Fensterung mit den entsprechenden Slidern und Feldern (2) ein. 
3. Stellen Sie den Schwellenwert für das Gewebe (3) ein. Mit der Checkbox "Show Threshold" wird der Schwellenwert in der Ansicht (A) dargestellt. 
4. Aktualisieren Sie die 3D-Darstellung mit dem Button "Update 3D View" (4). 
5. Drehen Sie die 3D-Darstellung mit den Buttons (5). Durch das Würfelsymbol können Sie die 3D-Darstellung zurücksetzen.
6. Der erste Arbeitsschritt ist abgeschlossen, wenn ein grüner Haken (i) erscheint.

#### Bohrung planen

Planen Sie die Bohrung in diesem Arbeitsschritt:

![GUI](./Docs/Gui/Folie2.PNG)

1. Setzen Sie den Startpunkt der Bohrung in der 3D-Darstellung (B) durch Klicken oder manuelle Eingabe der Koordinaten (2). 
2. Bestätigen Sie mit "Select Start" (3). 
3. Setzen Sie den Endpunkt in der Schichtansicht (A) und bearbeiten Sie die Koordinaten (4) sowie die Länge des Bohrkanals (5). 
4. Bestätigen Sie mit "Select End" (6). 
5. Der Arbeitsschritt ist abgeschlossen, wenn ein grüner Haken (ii) erscheint.

#### Bohrung verifizieren

Verifizieren Sie die geplante Bohrung in diesem Arbeitsschritt:

![GUI](./Docs/Gui/Folie3.PNG)

1. Wählen Sie zwischen verschiedenen Ansichten in (A) mit den "Radio Buttons" (1) aus. 
2. Wählen Sie zwischen verschiedenen Ansichten in (B) mit den "Radio Buttons" (2) aus.
3. Verifizieren Sie die Bohrung in den Ansichten (A) und (B).
4. Der Arbeitsschritt ist abgeschlossen, wenn ein grüner Haken (iii) erscheint.

#### Bohrschablone berechnen

Planen Sie die Bohrschablone im letzten Arbeitsschritt:

![GUI](./Docs/Gui/Folie4.PNG)

1. Stellen Sie den Bohrkanaldurchmesser mit dem Feld (1) ein.
2. Speichern Sie die Koordinaten und den Durchmesser mit "Save Position" (2) in einer .txt Datei. 
3. Speichern Sie die Bohrschablone mit "Save Template" (3) in einer .raw Datei. 
4. Überprüfen Sie die erstellten Dateien. 
5. Der Arbeitsschritt ist abgeschlossen, wenn ein grüner Haken (iv) erscheint.

Das Programm kann nach Abschluss aller Arbeitsschritte beendet werden.

### Planung

Zur Planung des Projektes wurde ein Pflichtenheft erstellt. Es ist unter folgendem Link verfügbar: https://tim.hess.pages.rwth-aachen.de/sweidmt_ss2023_tim_hess/Pflichtenheft.pdf

Weiterhin wurden folgende Dokumente verwendet:
* Folien der Vorlesung SWEIDMT
* Übungsblätter der Vorlesung SWEIDMT
* IEC 62304 - Medical device software - Software life cycle processes
* ISO 14971 - Medical devices - Application of risk management to medical devices
* ISO 13485 - Medical devices - Quality management systems
